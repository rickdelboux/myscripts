USE SageEMS;



SELECT User_Type = CASE mmbrp.[type]
                       WHEN 'G' THEN
                           'Windows Group'
                       WHEN 'S' THEN
                           'SQL User'
                       WHEN 'U' THEN
                           'Windows User'
				ELSE ''
                   END,
       Database_User_Name = mmbrp.[name],
       Login_Name = ul.[name],
       DB_Role = rolp.[name]
	   ,rolp.is_fixed_role
	 
FROM sys.database_role_members mmbr  -- The Role OR members associations table
 LEFT JOIN  sys.database_principals rolp on rolp.[principal_id] = mmbr.[role_principal_id]      -- The DB Roles names table
 LEFT join  sys.database_principals mmbrp on mmbrp.[principal_id] = mmbr.[member_principal_id]  -- The Role members table (database users)
 LEFT JOIN  sys.server_principals ul ON  ul.[sid] = mmbrp.[sid]      -- The Login accounts table
WHERE UPPER(mmbrp.[type]) IN ('G','U','G' )  --S = sql user, U = Windows User , G = Windows Group
      -- No need for these system account types
      AND UPPER(mmbrp.[name]) NOT IN ( 'SYS', 'INFORMATION_SCHEMA' )
      --AND rolp.[name] LIKE '%' + @dbRole + '%'
     --AND mmbrp.[name] = 'aws_etl'
ORDER BY Login_Name