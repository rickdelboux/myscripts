SELECT dopm.physical_memory_in_use_kb
     , dopm.large_page_allocations_kb
     , dopm.locked_page_allocations_kb
     , dopm.total_virtual_address_space_kb
     , dopm.virtual_address_space_reserved_kb
     , dopm.virtual_address_space_committed_kb
     , dopm.virtual_address_space_available_kb
     , dopm.page_fault_count
     , dopm.memory_utilization_percentage
     , dopm.available_commit_limit_kb
     , dopm.process_physical_memory_low
     , dopm.process_virtual_memory_low
FROM sys.dm_os_process_memory dopm;

--https://www.sqlservergeeks.com/sys-dm_os_process_memory/

SELECT physical_memory_in_use_kb / 1024 AS [Physical Memory In Use (MB)]
,locked_page_allocations_kb / 1024 AS [Locked Page In Memory Allocations (MB)]
,memory_utilization_percentage AS [Memory Utilization Percentage]
,available_commit_limit_kb / 1024 AS [Available Commit Limit (MB)]
,CASE WHEN process_physical_memory_low = 0 THEN 'No Memory Pressure Detected' ELSE 'Memory Low' END AS 'Process Physical Memory'
,CASE WHEN process_virtual_memory_low = 0 THEN 'No Memory Pressure Detected' ELSE 'Memory Low' END AS 'Process Virtual Memory'
,CURRENT_TIMESTAMP AS [Current Date Time]
FROM sys.dm_os_process_memory
OPTION (RECOMPILE);
GO