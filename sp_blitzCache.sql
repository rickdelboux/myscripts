USE [master]
GO

DECLARE @RC int
DECLARE @Help bit
DECLARE @Top int
DECLARE @SortOrder varchar(50)
DECLARE @UseTriggersAnyway bit
DECLARE @ExportToExcel bit
DECLARE @ExpertMode tinyint
DECLARE @OutputType varchar(20)
DECLARE @OutputServerName nvarchar(258)
DECLARE @OutputDatabaseName nvarchar(258)
DECLARE @OutputSchemaName nvarchar(258)
DECLARE @OutputTableName nvarchar(258)
DECLARE @ConfigurationDatabaseName nvarchar(128)
DECLARE @ConfigurationSchemaName nvarchar(258)
DECLARE @ConfigurationTableName nvarchar(258)
DECLARE @DurationFilter decimal(38,4)
DECLARE @HideSummary bit
DECLARE @IgnoreSystemDBs bit
DECLARE @OnlyQueryHashes varchar(max)
DECLARE @IgnoreQueryHashes varchar(max)
DECLARE @OnlySqlHandles varchar(max)
DECLARE @IgnoreSqlHandles varchar(max)
DECLARE @QueryFilter varchar(10)
DECLARE @DatabaseName nvarchar(128)
DECLARE @StoredProcName nvarchar(128)
DECLARE @SlowlySearchPlansFor nvarchar(4000)
DECLARE @Reanalyze bit
DECLARE @SkipAnalysis BIT
DECLARE @BringThePain BIT
DECLARE @MinimumExecutionCount INT
DECLARE @Debug BIT
DECLARE @CheckDateOverride DATETIMEOFFSET(7)
DECLARE @MinutesBack INT
DECLARE @Version VARCHAR(30)
DECLARE @VersionDate DATETIME
DECLARE @VersionCheckMode BIT

-- TODO: Set parameter values here.

EXECUTE @RC = [dbo].[sp_BlitzCache] 
	@Top = 100,
	 @SortOrder = 'Avg Duration (ms)',
    @ExpertMode = 1,
	@BringThePain  = 1


GO


