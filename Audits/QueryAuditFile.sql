
--enterprice editions only 
SELECT DATEADD(hh, DATEDIFF(hh, GETUTCDATE(), CURRENT_TIMESTAMP), event_time) AS corrected_time
     , action_id
     , session_server_principal_name
     , server_instance_name
     , database_name
     , schema_name
     , object_name
     , statement
    , file_name
   	
FROM sys.fn_get_audit_file('E:\SQL\MSSQL14.MSSQLSERVER\MSSQL\Log\SQLAgentJob*.sqlaudit', DEFAULT, DEFAULT)
WHERE CHARINDEX('Test Audit',statement,0) > 0
ORDER BY corrected_time;
