USE [master]
GO

/****** Object:  Audit [SQLAgentJob]    Script Date: 2/08/2022 1:59:11 PM https://thomaslarock.com/2017/10/audit-sql-server-jobs/ ******/
CREATE SERVER AUDIT [SQLAgentJob]
TO FILE 
(	FILEPATH = N'E:\SQL\MSSQL14.MSSQLSERVER\MSSQL\Log\'
	,MAXSIZE = 30 MB
	,MAX_FILES = 2
	,RESERVE_DISK_SPACE = OFF
) WITH (QUEUE_DELAY = 1000, ON_FAILURE = CONTINUE, AUDIT_GUID = '24b4ba86-034d-458f-bc1b-4dbc00f74546')
ALTER SERVER AUDIT [SQLAgentJob] WITH (STATE = ON)
GO



SELECT DATEADD(hh, DATEDIFF(hh, GETUTCDATE(), CURRENT_TIMESTAMP), event_time )

as corrected_time,

action_id ,

session_server_principal_name,

server_instance_name ,

database_name ,

schema_name ,

object_name ,

statement ,

file_name FROM sys.fn_get_audit_file( 'C:\*.sqlaudit' , DEFAULT , DEFAULT)