 --https://docs.microsoft.com/en-us/answers/questions/786836/page-density-in-sql-server-database.html
 
 SELECT OBJECT_SCHEMA_NAME(ips.object_id) AS schema_name,
        OBJECT_NAME(ips.object_id) AS object_name,
        i.name AS index_name,
        i.type_desc AS index_type,
        ips.avg_page_space_used_in_percent,
        ips.avg_fragmentation_in_percent,
        ips.page_count,
        ips.alloc_unit_type_desc,
        ips.ghost_record_count
 FROM sys.dm_db_index_physical_stats(DB_ID(), default, default, default, 'SAMPLED') AS ips
 INNER JOIN sys.indexes AS i 
 ON ips.object_id = i.object_id
    AND
    ips.index_id = i.index_id
	--WHERE ips.database_id = 8
 ORDER BY page_count DESC;