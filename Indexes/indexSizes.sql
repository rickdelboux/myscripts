-- Ensure a USE  statement has been executed first.
USE SageEMS
SELECT
    i.[name] AS IndexName,
    t.[name] AS TableName,
    SUM(s.[used_page_count]) * 8 AS IndexSizeKB,
	(  SUM(s.[used_page_count]) * 8 ) /1024 AS IndexSizeMB,
	(	(  SUM(s.[used_page_count]) * 8 ) /1024 ) /1024 AS  IndexSizeGig
FROM sys.dm_db_partition_stats AS s
INNER JOIN sys.indexes AS i ON s.[object_id] = i.[object_id]
    AND s.[index_id] = i.[index_id]
INNER JOIN sys.tables t ON t.OBJECT_ID = i.object_id
WHERE i.[name] LIKE '%IX_tbBill_Invoice_NMI_Item_Code_ItemTotal%'
GROUP BY i.[name], t.[name]
ORDER BY i.[name], t.[name]
