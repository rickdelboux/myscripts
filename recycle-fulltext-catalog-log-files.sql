zzz
--https://www.sqlskills.com/blogs/jonathan/recycle-fulltext-catalog-log-files/
USE ERMPower_MSCRM
EXEC sp_fulltext_recycle_crawl_log @ftcat = 'CRMFullTextCatalog'

USE SageEMS
EXEC sp_fulltext_recycle_crawl_log @ftcat = 'WebPortalAllAccounts'