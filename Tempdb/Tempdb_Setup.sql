/* FAILOVERS
You can do this in the Management studio if you prefer point and click. IMPORTANT you have to be on the server you are failing another server's DB's to
IE

if AG PNAA04 (SimEnergyProd and Trading) is on PNSQL27 and it should be on PNSQL26 then

ON PNSQL26 run.

  ALTER AVAILABILITY GROUP [PNAAG04] FAILOVER

If you want PNSQL27 on PNSQL26 you would then 

ON PNSQL26 run. 

 ALTER AVAILABILITY GROUP [PNAAG03] FAILOVER

 AND Visa Versa

 EG to get AG PNAA04 currently on PNSQL26 as its Primary to PNSQL27

 ON PNSQL27 run 

  ALTER AVAILABILITY GROUP [PNAAG04] FAILOVER

Changing tempdb will follow the same steps as patching that has a restart 
see https://www.sqlshack.com/apply-sql-server-patches-or-cumulative-updates-in-sql-server-always-on-availability-groups/
for details

Steps 

1 Kick all users off - 

DECLARE @command varchar(1000) 
SELECT @command = 'USE ? ALTER DATABASE CURRENT SET SINGLE_USER WITH ROLLBACK AFTER 60' 
EXEC sp_MSforeachdb @command 

(later remember  ALTER DATABASE YourDB SET MULTI_USER)

2 disable job on pnsql26  ADMIN - Force Failover AG PNAAG04


3 First failover PNAA04 to get AG PNAA04 currently on PNSQL26 as its Primary to PNSQL27

 ON PNSQL27 run 

  ALTER AVAILABILITY GROUP [PNAAG04] FAILOVER

so now all DB's are on PNSQL27 as primary replica

4  On PNSQL27 change the failover mode from Automatic to Manual like the below screenshot (see link).  pnsql28 IS ALWAYS MANUAL 
	DO THE SAME on pnsql26

It ensures that no automatic failover happens to the secondary replica in case of any issue on the primary replica while we apply the patches.
If you suspend the data movement from the primary replica, it suspends data movement for all secondary replicas. THIS is what is wanted as all 3 servers need to be done

so run 
SELECT 'ALTER DATABASE [' + dbcs.database_name + '] SET HADR SUSPEND'
FROM master.sys.availability_groups AS AG
  LEFT OUTER JOIN master.sys.dm_hadr_availability_group_states as agstates
    ON AG.group_id = agstates.group_id
  INNER JOIN master.sys.availability_replicas AS AR
    ON AG.group_id = AR.group_id
  INNER JOIN master.sys.dm_hadr_availability_replica_states AS arstates
    ON AR.replica_id = arstates.replica_id AND arstates.is_local = 1
  INNER JOIN master.sys.dm_hadr_database_replica_cluster_states AS dbcs
    ON arstates.replica_id = dbcs.replica_id
  LEFT OUTER JOIN master.sys.dm_hadr_database_replica_states AS dbrs
    ON dbcs.replica_id = dbrs.replica_id AND dbcs.group_database_id = dbrs.group_database_id
ORDER BY AG.name ASC, dbcs.database_name
and execute commands 

5 apply tempdb changes to PNSQL28

6 Restart PNSQL28 
7 Check changes are applied

8  PNSQL26 Apply tempdb changes 
9 Restart PNSQL26
10 Check changes are applied

11 Manully Failover PNSQL27 TO PNSQL26 

 ON PNSQL26 run 

  ALTER AVAILABILITY GROUP [PNAAG04] FAILOVER
  ALTER AVAILABILITY GROUP [PNAAG03] FAILOVER

this gets all AG's on to PNSQL26 (you may have to resume data movement  on 27 so get the AG synched and then suspend data movement again on 26)

12 On PSQL27 Apply tempdb changes 
13 Restart PNSQL27
14 Check changes are applied
15 resume data movement


so run  
SELECT 'ALTER DATABASE [' + dbcs.database_name + '] SET HADR RESUME'
FROM master.sys.availability_groups AS AG
  LEFT OUTER JOIN master.sys.dm_hadr_availability_group_states as agstates
    ON AG.group_id = agstates.group_id
  INNER JOIN master.sys.availability_replicas AS AR
    ON AG.group_id = AR.group_id
  INNER JOIN master.sys.dm_hadr_availability_replica_states AS arstates
    ON AR.replica_id = arstates.replica_id AND arstates.is_local = 1
  INNER JOIN master.sys.dm_hadr_database_replica_cluster_states AS dbcs
    ON arstates.replica_id = dbcs.replica_id
  LEFT OUTER JOIN master.sys.dm_hadr_database_replica_states AS dbrs
    ON dbcs.replica_id = dbrs.replica_id AND dbcs.group_database_id = dbrs.group_database_id
ORDER BY AG.name ASC, dbcs.database_name
and execute commands 


16 Once it -PNSQL27 - is green go to step 18
17 at this point we just need to failover PNAAG03 to PNSQL27 leaving PNSQL26 with PNAAG04 (SimEnergyProd and Trading)
so on PNSQL27
 ALTER AVAILABILITY GROUP [PNAAG03] FAILOVER

18 check AG is all green and tempdb changes are applied to all 3 servers.

then on all servers run

DECLARE @command varchar(1000) 
SELECT @command = 'USE ? ALTER DATABASE CURRENT SET MULTI_USER ' 
EXEC sp_MSforeachdb @command 

enable able job on pnsql26  ADMIN - Force Failover AG PNAAG04

*/



/******************************************TEMPDB CHANGES ************************************************************************************/

 
USE [master]; 
GO 


--- set all 3 servers the same Disk Size 

alter database tempdb modify file (name='tempdev', size = 585728MB);
GO
 
/* Adding 8 additional files
https://docs.microsoft.com/en-us/sql/relational-databases/databases/tempdb-database?view=sql-server-ver16

The number of secondary data files depends on the number of (logical) processors on the machine. 
              ----------------------------
As a general rule, if the number of logical processors is less than or equal to eight, 
use the same number of data files as logical processors. 

If the number of logical processors is greater than eight, use eight data files. 

email for nick 
I�ve confirmed it includes all database files, so .mdf and .ndf. SQL server doesn�t support parallel log streaming, so you can put .ldf on any VMDK and SCSI controller as you see fit.

That said, the layout will include one .mdf and seven .ndf files, like so:

  SCSI Controller 1 / VMDK 1
K:\SQL\DB\tempdb.mdf
K:\SQL\DB\tempdb_mssql_1.ndf  

SCSI Controller 2 / VMDK 2
L:\SQL\DB\tempdb_mssql_2.ndf
L:\SQL\DB\tempdb_mssql_3.ndf

SCSI Controller 3 / VMDK 3
M:\SQL\DB\tempdb_mssql_4.ndf
M:\SQL\DB\tempdb_mssql_5.ndf

SCSI Controller 4 / VMDK 4
N:\SQL\DB\tempdb_mssql_6.ndf
N:\SQL\DB\tempdb_mssql_7.ndf






*/
 
USE [master];

--try ALTER DATABASE [tempdb] REMOVE FILE  first after a restart of SQL Service --

---need a restart SQL Service to empty files and then 
--try ALTER DATABASE [tempdb] REMOVE FILE
-- if errors try DBCC SHRINKFILE
-- and then DBCC command
-- and then ALTER DATABASE [tempdb] ADD FILE 


--pnsql27 

--might need this ----------------------------------------------------------------
--DBCC SHRINKFILE('temp10', EMPTYFILE)  
--DBCC SHRINKFILE('temp2', EMPTYFILE) 
--DBCC SHRINKFILE('temp3', EMPTYFILE) 
--DBCC SHRINKFILE('temp4', EMPTYFILE) 
--DBCC SHRINKFILE('temp5', EMPTYFILE) 
--DBCC SHRINKFILE('temp6', EMPTYFILE) 
--DBCC SHRINKFILE('temp7', EMPTYFILE) 
--DBCC SHRINKFILE('temp8', EMPTYFILE) 
--DBCC SHRINKFILE('temp9', EMPTYFILE) 
-- then 
--DBCC DROPCLEANBUFFERS
--GO
--DBCC FREEPROCCACHE
--GO
--DBCC FREESESSIONCACHE
--GO
--DBCC FREESYSTEMCACHE ('ALL')
--GO
-- and now ALTER DATABASE [tempdb] REMOVE FILE  
------------------------------------------------------------------------------------------------

-- after a restart SQL Service this should work without the above
--ALTER DATABASE [tempdb] REMOVE FILE [temp10]
--ALTER DATABASE [tempdb] REMOVE FILE [temp2] 
--ALTER DATABASE [tempdb] REMOVE FILE [temp3]
--ALTER DATABASE [tempdb] REMOVE FILE [temp4]
--ALTER DATABASE [tempdb] REMOVE FILE [temp5]
--ALTER DATABASE [tempdb] REMOVE FILE [temp6]
--ALTER DATABASE [tempdb] REMOVE FILE [temp7]
--ALTER DATABASE [tempdb] REMOVE FILE [temp8]
--ALTER DATABASE [tempdb] REMOVE FILE [temp9]

--pnsql26 and 28 ---------------------------------------------------------------------------------------------------------------------------------

--might need this -----------------------------------------------------------------------------------
--DBCC SHRINKFILE('temp2', EMPTYFILE) 
--DBCC SHRINKFILE('temp3', EMPTYFILE) 
--DBCC SHRINKFILE('temp4', EMPTYFILE) 
--DBCC SHRINKFILE('temp5', EMPTYFILE) 
--DBCC SHRINKFILE('temp6', EMPTYFILE) 
--DBCC SHRINKFILE('temp7', EMPTYFILE) 
--DBCC SHRINKFILE('temp8', EMPTYFILE) 
--
--DBCC DROPCLEANBUFFERS
--GO
--DBCC FREEPROCCACHE
--GO
--DBCC FREESESSIONCACHE
--GO
--DBCC FREESYSTEMCACHE ('ALL')
--GO
-------------------------------------------------------------------------------------------------------------


/*Note the the Drive letters are currently up to and including Z so zI, zJ,zK are place holders for new drive letters
log file is on F:
G:\SQL\DB\tempdb.mdf

*/
GO
--pnsql27

ALTER DATABASE [tempdb] REMOVE FILE [temp10]
ALTER DATABASE [tempdb] REMOVE FILE [temp2] 
ALTER DATABASE [tempdb] REMOVE FILE [temp3]
ALTER DATABASE [tempdb] REMOVE FILE [temp4]
ALTER DATABASE [tempdb] REMOVE FILE [temp5]
ALTER DATABASE [tempdb] REMOVE FILE [temp6]
ALTER DATABASE [tempdb] REMOVE FILE [temp7]
ALTER DATABASE [tempdb] REMOVE FILE [temp8]
ALTER DATABASE [tempdb] REMOVE FILE [temp9]

---pnsql26 and 28
--ALTER DATABASE [tempdb] REMOVE FILE [temp2] 
--ALTER DATABASE [tempdb] REMOVE FILE [temp3]
--ALTER DATABASE [tempdb] REMOVE FILE [temp4]
--ALTER DATABASE [tempdb] REMOVE FILE [temp5]
--ALTER DATABASE [tempdb] REMOVE FILE [temp6]
--ALTER DATABASE [tempdb] REMOVE FILE [temp7]
--ALTER DATABASE [tempdb] REMOVE FILE [temp8]



-- all servers 

--G:\SQL\DB\tempdb.mdf already exists 
ALTER DATABASE [tempdb] ADD FILE (NAME = N'temp10', FILENAME = N'G:\SQL\DB\tempdb_mssql_10.ndf' , SIZE = 77825MB , FILEGROWTH = 256MB);

ALTER DATABASE [tempdb] ADD FILE (NAME = N'temp3', FILENAME = N'H:\SQL\DB\tempdb_mssql_2.ndf' , SIZE = 77825MB , FILEGROWTH = 256MB);
ALTER DATABASE [tempdb] ADD FILE (NAME = N'temp4', FILENAME = N'H:\SQL\DB\tempdb_mssql_3.ndf' , SIZE = 77825MB , FILEGROWTH = 256MB);

ALTER DATABASE [tempdb] ADD FILE (NAME = N'temp5', FILENAME = N'zI:\SQL\DB\tempdb_mssql_4.ndf' , SIZE = 77825MB , FILEGROWTH = 256MB);
ALTER DATABASE [tempdb] ADD FILE (NAME = N'temp6', FILENAME = N'zI:\SQL\DB\tempdb_mssql_5.ndf' , SIZE = 77825MB , FILEGROWTH = 256MB);

ALTER DATABASE [tempdb] ADD FILE (NAME = N'temp7', FILENAME = N'zJ:\SQL\DB\tempdb_mssql_6.ndf' , SIZE = 77825MB , FILEGROWTH = 256MB);
ALTER DATABASE [tempdb] ADD FILE (NAME = N'temp8', FILENAME = N'zJ:\SQL\DB\tempdb_mssql_7.ndf' , SIZE = 77825MB , FILEGROWTH = 256MB);


GO