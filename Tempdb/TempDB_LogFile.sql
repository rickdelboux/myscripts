SELECT name
,recovery_model_desc
,log_reuse_wait_desc  --ACTIVE_TRANSACTION = BAD
FROM sys.databases
WHERE name = 'tempdb'

--Restart SQL Server Service to re-create tempdb database and resolve the issue

--2- find a list of Open transactions on sql server instance and do the Following:

--using below query get a list of active sessions:
SELECT *
FROM sys.dm_exec_sessions des
WHERE des.status NOT IN ('sleeping')

DBCC OPENTRAN WITH TABLERESULTS

--KILL [spid]


--DBCC SHRINKFILE (templog,100)