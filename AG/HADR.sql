SELECT name, role_desc, state_desc FROM sys.database_mirroring_endpoints
SELECT name, port FROM sys.tcp_endpoints  
/*name	port
Dedicated Admin Connection	0
TSQL Default TCP	0
SSBEndpoint	4022
Hadr_endpoint	5022*/
SELECT 'Metadata Check';
SELECT EP.name, SP.STATE, 
   CONVERT(nvarchar(38), suser_name(SP.grantor_principal_id)) 
      AS GRANTOR, 
   SP.TYPE AS PERMISSION,
   CONVERT(nvarchar(46),suser_name(SP.grantee_principal_id)) 
      AS GRANTEE 
   FROM sys.server_permissions SP , sys.endpoints EP
   WHERE SP.major_id = EP.endpoint_id
   ORDER BY Permission,grantor, grantee; 
/*
27
name	STATE	GRANTOR	PERMISSION	GRANTEE
Hadr_endpoint	G	ERM\wardyit_admin	CO  	ERM\svc_PNAAG02SQL
SSBEndpoint	G	ERM\wardyit_admin	CO  	public
TSQL Local Machine	G	sa	CO  	public
TSQL Named Pipes	G	sa	CO  	public
TSQL Default TCP	G	sa	CO  	public
TSQL Default VIA	G	sa	CO  	public
26
name	STATE	GRANTOR	PERMISSION	GRANTEE
Hadr_endpoint	G	ERM\wardyit_admin	CO  	ERM\svc_PNAAG02SQL
SSBEndpoint	G	ERM\wardyit_admin	CO  	public
TSQL Local Machine	G	sa	CO  	public
TSQL Named Pipes	G	sa	CO  	public
TSQL Default TCP	G	sa	CO  	public
TSQL Default VIA	G	sa	CO  	public

28
name	STATE	GRANTOR	PERMISSION	GRANTEE
Hadr_endpoint	G	ERM\wardyit_admin	CO  	ERM\svc_PNAAG02SQL
SSBEndpoint	G	ERM\wardyit_admin	CO  	public
TSQL Local Machine	G	sa	CO  	public
TSQL Named Pipes	G	sa	CO  	public
TSQL Default TCP	G	sa	CO  	public
TSQL Default VIA	G	sa	CO  	public
*/