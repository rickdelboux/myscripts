SELECT [XML Data]
     , [XML Data].value('(/event[@name=''error_reported'']/@timestamp)[1]', 'DATETIME') AS [Timestamp]
     , DATEADD(
                  MI
                , DATEDIFF(MI, GETUTCDATE(), GETDATE())
                , [XML Data].value('(/event[@name=''error_reported'']/@timestamp)[1]', 'DATETIME')
              ) AS LOCAL_Time
     , [XML Data].value('(/event/action[@name=''database_name'']/value)[1]', 'varchar(max)') AS [Database]
     , [XML Data].value('(/event/data[@name=''message'']/value)[1]', 'varchar(max)') AS [Message]
     , [XML Data].value('(/event/action[@name=''sql_text'']/value)[1]', 'varchar(max)') AS [Statement]
     , [XML Data].value('(/event/action[@name=''server_principal_name'']/value)[1]', 'varchar(max)') AS [server_principal_name]
     , [XML Data].value('(/event/action[@name=''client_app_name'']/value)[1]', 'varchar(max)') AS client_app_name
FROM
(
    SELECT object_name AS [Event]
         , CONVERT(XML, event_data) AS [XML Data]
    FROM sys.fn_xe_file_target_read_file(
                                            'E:\SQL\MSSQL14.MSSQLSERVER\MSSQL\Log\CRM_Error_reported_0_133071635811310000.xel'
                                          , NULL
                                          , NULL
                                          , NULL
                                        )
) AS FailedQueries;