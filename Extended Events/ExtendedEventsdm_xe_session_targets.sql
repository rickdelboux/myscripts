USE master
--- file name 
SELECT s.name
     , t.target_name
     , CAST(t.target_data AS XML) AS [XML-Cast]
FROM sys.dm_xe_session_targets AS t
JOIN sys.dm_xe_sessions AS s ON s.address = t.event_session_address
WHERE s.name = 'SAGE_TimeOuts';


---utc time??
SELECT [XML Data]
     , [XML Data].value('(/event[@name=''rpc_completed'']/@timestamp)[1]', 'DATETIME') AS [Timestamp]

	, DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), [XML Data].value('(/event[@name=''rpc_completed'']/@timestamp)[1]', 'DATETIME')) AS LocalTime
     , [XML Data].value('(/event/action[@name=''database_name'']/value)[1]', 'varchar(max)') AS [Database]
     , [XML Data].value('(/event/data[@name=''message'']/value)[1]', 'varchar(max)') AS [Message]
     , [XML Data].value('(/event/action[@name=''sql_text'']/value)[1]', 'varchar(max)') AS [Statement]
	  , [XML Data].value('(/event/data[@name=''object_name'']/value)[1]', 'varchar(max)') AS [object_name]
	  , [XML Data].value('(/event/data[@name=''statement'']/value)[1]', 'varchar(max)') AS [statement]
FROM
(
    SELECT object_name AS [Event]
         , CONVERT(XML, event_data) AS [XML Data]
    FROM sys.fn_xe_file_target_read_file(
                                            'E:\SQL\MSSQL14.MSSQLSERVER\MSSQL\Log\TimeOuts_0_133048129651670000.xel'
                                          , NULL
                                          , NULL
                                          , NULL
                                        )
) AS FailedQueries;


select ((884487032/1000)/60) /60