
CREATE EVENT SESSION [Error_reported] ON SERVER
ADD EVENT sqlserver.error_reported
(
    ACTION
    (
          sqlserver.server_instance_name   /* good practice for multi server querying */
        , sqlserver.client_app_name        /* helps locate the calling app */
        , sqlserver.client_hostname        /* calling computer name */
        , sqlserver.server_principal_name  /* can be switched to a user */
        , sqlserver.database_id            /* can be switched to a database_name */
        , sqlserver.sql_text               /* grab calling parameters from input buffer */
        , sqlserver.tsql_stack             /* get the whole stack for parsing later */
    )
    WHERE
    (
        severity > 10
		/* Please test and provide additional filters! */
		AND  (sqlserver.client_app_name NOT LIKE 'SQLAgent%')
        
    )
)
 ADD TARGET package0.event_file
    (SET filename = N'E:\SQL\MSSQL14.MSSQLSERVER\MSSQL\Log\Error_reported.xel', max_file_size = (200))
    