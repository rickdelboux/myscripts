
-- -------------------basic checks----------------------------------------------------------
-- SageEMS and SageLFH should have both as 1
-- if these are 0 then they should be enabled again (msdn doco has these commands)
-- these check results have never changed so far.  

USE master
SELECT name, is_broker_enabled, is_master_key_encrypted_by_server FROM sys.databases 

USE SageEMS
SELECT * FROM sys.symmetric_keys

USE SageEMS_LFH
SELECT * FROM sys.symmetric_keys

--------------------------------------- check and fix Queues  ------------------------------------------------------------

USE SageEMS_LFH
SELECT * FROM [dbo].[Q_Tran_Process]  --- first check 
-- if there are an increasing number of row in this queue - run the following Alter Queue 

USE SageEMS_LFH
ALTER QUEUE [dbo].[Q_Tran_Process] WITH STATUS = ON

--- also
USE SageEMS
SELECT * FROM [dbo].[Q_Asyn_Request2] 

USE SageEMS 
 ALTER QUEUE [dbo].[Q_Asyn_request2] WITH STATUS = ON


--- also
USE SageEMS
SELECT * FROM [dbo].[Q_Tran_Process] 

USE SageEMS 
 ALTER QUEUE [dbo].[Q_Tran_Process]  WITH STATUS = ON


--(not sure of the timing of the check IE when there are things in the queue. I dont get any log messaging )
-- this one to show error normal state is nothing  to show here  NOTE DB specific
USE SageEMS_lfh;
SELECT * FROM sys.transmission_queue;

USE SageEMS
-- this one always shows one dated 13-5-2021 (possible to delete this ?)
SELECT * FROM sys.transmission_queue

-- if the transmission_queue queue(s) are not working try this as it has fixed it in the past
USE SageEMS
ALTER MASTER KEY FORCE REGENERATE WITH ENCRYPTION BY PASSWORD = '...'

USE SageEMS_LHF
ALTER MASTER KEY FORCE REGENERATE WITH ENCRYPTION BY PASSWORD = '...'

-- if still not working then 

USE SageEMS
OPEN MASTER KEY DECRYPTION BY PASSWORD = '<password>'
ALTER MASTER KEY ADD ENCRYPTION BY SERVICE MASTER KEY
CLOSE MASTER KEY

Use SageEMS_LFH

OPEN MASTER KEY DECRYPTION BY PASSWORD = '<password>'
ALTER MASTER KEY ADD ENCRYPTION BY SERVICE MASTER KEY
CLOSE MASTER KEY
