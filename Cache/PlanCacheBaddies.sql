USE master
;WITH baddies
AS (SELECT query_hash,
           MIN(plan_handle) plan_handle,
           MIN(sql_handle) sql_handle,
           SUM(execution_count) Execution_Count,
           COUNT(*) Plans,
           SUM(total_elapsed_time) DURATION
    FROM sys.dm_exec_query_stats
    GROUP BY query_hash
	)
SELECT TOP 10
       Plans,
       Execution_Count,
       DURATION,
       DURATION / 1000 / Execution_Count PerRun,
       text
FROM baddies
    CROSS APPLY sys.dm_exec_sql_text(sql_handle)
    CROSS APPLY sys.dm_exec_query_plan(plan_handle) qp
--WHERE text LIKE '%spCRM_UpdateSimEnergyPricing%'
ORDER BY Plans DESC;

