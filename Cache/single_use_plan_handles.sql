USE master;
DROP TABLE IF EXISTS #single_use_plan_handles;

CREATE TABLE #single_use_plan_handles
(
    plan_handle VARBINARY(64)
  , size_in_bytes INT
);


INSERT INTO #single_use_plan_handles
SELECT plan_handle
     , size_in_bytes
FROM sys.dm_exec_cached_plans
WHERE usecounts = 1;


SELECT COUNT(1) number_of_queries
     , a.query_hash
     , SUM(CONVERT(BIGINT, b.size_in_bytes)) size_in_cache_bytes
	  , MAX(b.size_in_bytes) as size_of_each_bytes
	  , SUM(CONVERT(BIGINT, b.size_in_bytes)) /1024 AS size_in_cache_Kbytes
	    , (SUM(CONVERT(BIGINT, b.size_in_bytes)) /1024)/1024 AS size_in_cache_Mbytes
    
     , (
           SELECT TOP 1
                  b.text
           FROM sys.dm_exec_query_stats aa
           CROSS APPLY sys.dm_exec_sql_text(aa.sql_handle) b
           WHERE aa.query_hash = a.query_hash
       ) AS sqlcodeSample
FROM sys.dm_exec_query_stats a
INNER JOIN #single_use_plan_handles b ON a.plan_handle = b.plan_handle
GROUP BY a.query_hash
HAVING COUNT(1) > 1
ORDER BY 3 DESC;




--SELECT TOP 10
--       text
--	   ,sql_plan.query_plan
--FROM sys.dm_exec_query_stats a
--CROSS APPLY sys.dm_exec_sql_text(a.sql_handle) b
--CROSS APPLY sys.dm_exec_query_plan(a.plan_handle) as sql_plan
--WHERE a.query_hash = 0x85106486A07196BB;

