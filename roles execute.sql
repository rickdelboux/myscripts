
DROP TABLE IF EXISTS ##users 


CREATE TABLE ##users (
	[DB_NAME] [NVARCHAR](128) NULL,
	[User_Type] [VARCHAR](13) NULL,
	[Database_User_Name] [sysname]  NULL,
	[Login_Name] [sysname]  NULL,
	[DB_Role] [sysname]  NULL,
	[is_fixed_role] [BIT]  NULL,
	[Schema] [sysname] NULL,
	[Object] [NVARCHAR](128) NULL,
	[User] [sysname] NULL,
	[type] [CHAR](1) NULL,
	[type_desc] [NVARCHAR](60) NULL,
	[permission_name] [NVARCHAR](128) NULL,
	[state_desc] [NVARCHAR](60) NULL
)
  

 use CDCTest
	
;WITH users
AS (
   
SELECT User_Type = CASE members.[type]
                       WHEN 'G' THEN
                           'Windows Group'
                       WHEN 'S' THEN
                           'SQL User'
                       WHEN 'U' THEN
                           'Windows User'
                   END
     , Database_User_Name = members.[name]
     , Login_Name = ul.[name]
     , roles.name AS DB_Role
     , roles.is_fixed_role
FROM sys.database_role_members AS database_role_members --mmbr
LEFT JOIN sys.database_principals AS roles -- rolp
    ON database_role_members.role_principal_id = roles.principal_id
LEFT JOIN sys.database_principals AS members -- mmbrp
    ON database_role_members.member_principal_id = members.principal_id
LEFT JOIN sys.server_principals ul ON ul.sid = members.sid
   )
    , objects
AS (
   SELECT s.name AS [Schema]
        , o.name AS Object
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   LEFT JOIN sys.objects o ON dp.major_id = o.object_id
   LEFT JOIN sys.schemas s ON o.schema_id = s.schema_id
   LEFT JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
  
   UNION ALL
   SELECT s.name AS [Schema]
        , NULL
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   JOIN sys.schemas s ON dp.major_id = s.schema_id
   JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
   )
INSERT ##users
(
    DB_NAME
  , User_Type
  , Database_User_Name
  , Login_Name
  , DB_Role
  , is_fixed_role
  , [Schema]
  , Object
  , [User]
  , type
  , type_desc
  , permission_name
  , state_desc
)
SELECT DB_NAME() AS DB_NAME
     , users.User_Type
     , users.Database_User_Name
     , users.Login_Name
     , users.DB_Role
     , users.is_fixed_role
     , o.[Schema]
     , o.Object
     , o.[User]
     , o.type
     , o.type_desc
     , o.permission_name
     , o.state_desc
FROM users
LEFT JOIN objects o ON o.[User] = users.Login_Name
 use ERMPower_MSCRM
	
;WITH users
AS (
   
SELECT User_Type = CASE members.[type]
                       WHEN 'G' THEN
                           'Windows Group'
                       WHEN 'S' THEN
                           'SQL User'
                       WHEN 'U' THEN
                           'Windows User'
                   END
     , Database_User_Name = members.[name]
     , Login_Name = ul.[name]
     , roles.name AS DB_Role
     , roles.is_fixed_role
FROM sys.database_role_members AS database_role_members --mmbr
LEFT JOIN sys.database_principals AS roles -- rolp
    ON database_role_members.role_principal_id = roles.principal_id
LEFT JOIN sys.database_principals AS members -- mmbrp
    ON database_role_members.member_principal_id = members.principal_id
LEFT JOIN sys.server_principals ul ON ul.sid = members.sid
   )
    , objects
AS (
   SELECT s.name AS [Schema]
        , o.name AS Object
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   LEFT JOIN sys.objects o ON dp.major_id = o.object_id
   LEFT JOIN sys.schemas s ON o.schema_id = s.schema_id
   LEFT JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
  
   UNION ALL
   SELECT s.name AS [Schema]
        , NULL
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   JOIN sys.schemas s ON dp.major_id = s.schema_id
   JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
   )
INSERT ##users
(
    DB_NAME
  , User_Type
  , Database_User_Name
  , Login_Name
  , DB_Role
  , is_fixed_role
  , [Schema]
  , Object
  , [User]
  , type
  , type_desc
  , permission_name
  , state_desc
)
SELECT DB_NAME() AS DB_NAME
     , users.User_Type
     , users.Database_User_Name
     , users.Login_Name
     , users.DB_Role
     , users.is_fixed_role
     , o.[Schema]
     , o.Object
     , o.[User]
     , o.type
     , o.type_desc
     , o.permission_name
     , o.state_desc
FROM users
LEFT JOIN objects o ON o.[User] = users.Login_Name
 use ERMPower_SAGEINTEROP
	
;WITH users
AS (
   
SELECT User_Type = CASE members.[type]
                       WHEN 'G' THEN
                           'Windows Group'
                       WHEN 'S' THEN
                           'SQL User'
                       WHEN 'U' THEN
                           'Windows User'
                   END
     , Database_User_Name = members.[name]
     , Login_Name = ul.[name]
     , roles.name AS DB_Role
     , roles.is_fixed_role
FROM sys.database_role_members AS database_role_members --mmbr
LEFT JOIN sys.database_principals AS roles -- rolp
    ON database_role_members.role_principal_id = roles.principal_id
LEFT JOIN sys.database_principals AS members -- mmbrp
    ON database_role_members.member_principal_id = members.principal_id
LEFT JOIN sys.server_principals ul ON ul.sid = members.sid
   )
    , objects
AS (
   SELECT s.name AS [Schema]
        , o.name AS Object
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   LEFT JOIN sys.objects o ON dp.major_id = o.object_id
   LEFT JOIN sys.schemas s ON o.schema_id = s.schema_id
   LEFT JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
  
   UNION ALL
   SELECT s.name AS [Schema]
        , NULL
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   JOIN sys.schemas s ON dp.major_id = s.schema_id
   JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
   )
INSERT ##users
(
    DB_NAME
  , User_Type
  , Database_User_Name
  , Login_Name
  , DB_Role
  , is_fixed_role
  , [Schema]
  , Object
  , [User]
  , type
  , type_desc
  , permission_name
  , state_desc
)
SELECT DB_NAME() AS DB_NAME
     , users.User_Type
     , users.Database_User_Name
     , users.Login_Name
     , users.DB_Role
     , users.is_fixed_role
     , o.[Schema]
     , o.Object
     , o.[User]
     , o.type
     , o.type_desc
     , o.permission_name
     , o.state_desc
FROM users
LEFT JOIN objects o ON o.[User] = users.Login_Name
 use ERMSales_MSCRM
	
;WITH users
AS (
   
SELECT User_Type = CASE members.[type]
                       WHEN 'G' THEN
                           'Windows Group'
                       WHEN 'S' THEN
                           'SQL User'
                       WHEN 'U' THEN
                           'Windows User'
                   END
     , Database_User_Name = members.[name]
     , Login_Name = ul.[name]
     , roles.name AS DB_Role
     , roles.is_fixed_role
FROM sys.database_role_members AS database_role_members --mmbr
LEFT JOIN sys.database_principals AS roles -- rolp
    ON database_role_members.role_principal_id = roles.principal_id
LEFT JOIN sys.database_principals AS members -- mmbrp
    ON database_role_members.member_principal_id = members.principal_id
LEFT JOIN sys.server_principals ul ON ul.sid = members.sid
   )
    , objects
AS (
   SELECT s.name AS [Schema]
        , o.name AS Object
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   LEFT JOIN sys.objects o ON dp.major_id = o.object_id
   LEFT JOIN sys.schemas s ON o.schema_id = s.schema_id
   LEFT JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
  
   UNION ALL
   SELECT s.name AS [Schema]
        , NULL
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   JOIN sys.schemas s ON dp.major_id = s.schema_id
   JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
   )
INSERT ##users
(
    DB_NAME
  , User_Type
  , Database_User_Name
  , Login_Name
  , DB_Role
  , is_fixed_role
  , [Schema]
  , Object
  , [User]
  , type
  , type_desc
  , permission_name
  , state_desc
)
SELECT DB_NAME() AS DB_NAME
     , users.User_Type
     , users.Database_User_Name
     , users.Login_Name
     , users.DB_Role
     , users.is_fixed_role
     , o.[Schema]
     , o.Object
     , o.[User]
     , o.type
     , o.type_desc
     , o.permission_name
     , o.state_desc
FROM users
LEFT JOIN objects o ON o.[User] = users.Login_Name
 use Infoserver
	
;WITH users
AS (
   
SELECT User_Type = CASE members.[type]
                       WHEN 'G' THEN
                           'Windows Group'
                       WHEN 'S' THEN
                           'SQL User'
                       WHEN 'U' THEN
                           'Windows User'
                   END
     , Database_User_Name = members.[name]
     , Login_Name = ul.[name]
     , roles.name AS DB_Role
     , roles.is_fixed_role
FROM sys.database_role_members AS database_role_members --mmbr
LEFT JOIN sys.database_principals AS roles -- rolp
    ON database_role_members.role_principal_id = roles.principal_id
LEFT JOIN sys.database_principals AS members -- mmbrp
    ON database_role_members.member_principal_id = members.principal_id
LEFT JOIN sys.server_principals ul ON ul.sid = members.sid
   )
    , objects
AS (
   SELECT s.name AS [Schema]
        , o.name AS Object
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   LEFT JOIN sys.objects o ON dp.major_id = o.object_id
   LEFT JOIN sys.schemas s ON o.schema_id = s.schema_id
   LEFT JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
  
   UNION ALL
   SELECT s.name AS [Schema]
        , NULL
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   JOIN sys.schemas s ON dp.major_id = s.schema_id
   JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
   )
INSERT ##users
(
    DB_NAME
  , User_Type
  , Database_User_Name
  , Login_Name
  , DB_Role
  , is_fixed_role
  , [Schema]
  , Object
  , [User]
  , type
  , type_desc
  , permission_name
  , state_desc
)
SELECT DB_NAME() AS DB_NAME
     , users.User_Type
     , users.Database_User_Name
     , users.Login_Name
     , users.DB_Role
     , users.is_fixed_role
     , o.[Schema]
     , o.Object
     , o.[User]
     , o.type
     , o.type_desc
     , o.permission_name
     , o.state_desc
FROM users
LEFT JOIN objects o ON o.[User] = users.Login_Name
 use master
	
;WITH users
AS (
   
SELECT User_Type = CASE members.[type]
                       WHEN 'G' THEN
                           'Windows Group'
                       WHEN 'S' THEN
                           'SQL User'
                       WHEN 'U' THEN
                           'Windows User'
                   END
     , Database_User_Name = members.[name]
     , Login_Name = ul.[name]
     , roles.name AS DB_Role
     , roles.is_fixed_role
FROM sys.database_role_members AS database_role_members --mmbr
LEFT JOIN sys.database_principals AS roles -- rolp
    ON database_role_members.role_principal_id = roles.principal_id
LEFT JOIN sys.database_principals AS members -- mmbrp
    ON database_role_members.member_principal_id = members.principal_id
LEFT JOIN sys.server_principals ul ON ul.sid = members.sid
   )
    , objects
AS (
   SELECT s.name AS [Schema]
        , o.name AS Object
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   LEFT JOIN sys.objects o ON dp.major_id = o.object_id
   LEFT JOIN sys.schemas s ON o.schema_id = s.schema_id
   LEFT JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
  
   UNION ALL
   SELECT s.name AS [Schema]
        , NULL
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   JOIN sys.schemas s ON dp.major_id = s.schema_id
   JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
   )
INSERT ##users
(
    DB_NAME
  , User_Type
  , Database_User_Name
  , Login_Name
  , DB_Role
  , is_fixed_role
  , [Schema]
  , Object
  , [User]
  , type
  , type_desc
  , permission_name
  , state_desc
)
SELECT DB_NAME() AS DB_NAME
     , users.User_Type
     , users.Database_User_Name
     , users.Login_Name
     , users.DB_Role
     , users.is_fixed_role
     , o.[Schema]
     , o.Object
     , o.[User]
     , o.type
     , o.type_desc
     , o.permission_name
     , o.state_desc
FROM users
LEFT JOIN objects o ON o.[User] = users.Login_Name
 use MSCRM_CONFIG
	
;WITH users
AS (
   
SELECT User_Type = CASE members.[type]
                       WHEN 'G' THEN
                           'Windows Group'
                       WHEN 'S' THEN
                           'SQL User'
                       WHEN 'U' THEN
                           'Windows User'
                   END
     , Database_User_Name = members.[name]
     , Login_Name = ul.[name]
     , roles.name AS DB_Role
     , roles.is_fixed_role
FROM sys.database_role_members AS database_role_members --mmbr
LEFT JOIN sys.database_principals AS roles -- rolp
    ON database_role_members.role_principal_id = roles.principal_id
LEFT JOIN sys.database_principals AS members -- mmbrp
    ON database_role_members.member_principal_id = members.principal_id
LEFT JOIN sys.server_principals ul ON ul.sid = members.sid
   )
    , objects
AS (
   SELECT s.name AS [Schema]
        , o.name AS Object
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   LEFT JOIN sys.objects o ON dp.major_id = o.object_id
   LEFT JOIN sys.schemas s ON o.schema_id = s.schema_id
   LEFT JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
  
   UNION ALL
   SELECT s.name AS [Schema]
        , NULL
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   JOIN sys.schemas s ON dp.major_id = s.schema_id
   JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
   )
INSERT ##users
(
    DB_NAME
  , User_Type
  , Database_User_Name
  , Login_Name
  , DB_Role
  , is_fixed_role
  , [Schema]
  , Object
  , [User]
  , type
  , type_desc
  , permission_name
  , state_desc
)
SELECT DB_NAME() AS DB_NAME
     , users.User_Type
     , users.Database_User_Name
     , users.Login_Name
     , users.DB_Role
     , users.is_fixed_role
     , o.[Schema]
     , o.Object
     , o.[User]
     , o.type
     , o.type_desc
     , o.permission_name
     , o.state_desc
FROM users
LEFT JOIN objects o ON o.[User] = users.Login_Name
 use SageEMS
	
;WITH users
AS (
   
SELECT User_Type = CASE members.[type]
                       WHEN 'G' THEN
                           'Windows Group'
                       WHEN 'S' THEN
                           'SQL User'
                       WHEN 'U' THEN
                           'Windows User'
                   END
     , Database_User_Name = members.[name]
     , Login_Name = ul.[name]
     , roles.name AS DB_Role
     , roles.is_fixed_role
FROM sys.database_role_members AS database_role_members --mmbr
LEFT JOIN sys.database_principals AS roles -- rolp
    ON database_role_members.role_principal_id = roles.principal_id
LEFT JOIN sys.database_principals AS members -- mmbrp
    ON database_role_members.member_principal_id = members.principal_id
LEFT JOIN sys.server_principals ul ON ul.sid = members.sid
   )
    , objects
AS (
   SELECT s.name AS [Schema]
        , o.name AS Object
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   LEFT JOIN sys.objects o ON dp.major_id = o.object_id
   LEFT JOIN sys.schemas s ON o.schema_id = s.schema_id
   LEFT JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
  
   UNION ALL
   SELECT s.name AS [Schema]
        , NULL
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   JOIN sys.schemas s ON dp.major_id = s.schema_id
   JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
   )
INSERT ##users
(
    DB_NAME
  , User_Type
  , Database_User_Name
  , Login_Name
  , DB_Role
  , is_fixed_role
  , [Schema]
  , Object
  , [User]
  , type
  , type_desc
  , permission_name
  , state_desc
)
SELECT DB_NAME() AS DB_NAME
     , users.User_Type
     , users.Database_User_Name
     , users.Login_Name
     , users.DB_Role
     , users.is_fixed_role
     , o.[Schema]
     , o.Object
     , o.[User]
     , o.type
     , o.type_desc
     , o.permission_name
     , o.state_desc
FROM users
LEFT JOIN objects o ON o.[User] = users.Login_Name
 use SageEMS_LFH
	
;WITH users
AS (
   
SELECT User_Type = CASE members.[type]
                       WHEN 'G' THEN
                           'Windows Group'
                       WHEN 'S' THEN
                           'SQL User'
                       WHEN 'U' THEN
                           'Windows User'
                   END
     , Database_User_Name = members.[name]
     , Login_Name = ul.[name]
     , roles.name AS DB_Role
     , roles.is_fixed_role
FROM sys.database_role_members AS database_role_members --mmbr
LEFT JOIN sys.database_principals AS roles -- rolp
    ON database_role_members.role_principal_id = roles.principal_id
LEFT JOIN sys.database_principals AS members -- mmbrp
    ON database_role_members.member_principal_id = members.principal_id
LEFT JOIN sys.server_principals ul ON ul.sid = members.sid
   )
    , objects
AS (
   SELECT s.name AS [Schema]
        , o.name AS Object
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   LEFT JOIN sys.objects o ON dp.major_id = o.object_id
   LEFT JOIN sys.schemas s ON o.schema_id = s.schema_id
   LEFT JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
  
   UNION ALL
   SELECT s.name AS [Schema]
        , NULL
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   JOIN sys.schemas s ON dp.major_id = s.schema_id
   JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
   )
INSERT ##users
(
    DB_NAME
  , User_Type
  , Database_User_Name
  , Login_Name
  , DB_Role
  , is_fixed_role
  , [Schema]
  , Object
  , [User]
  , type
  , type_desc
  , permission_name
  , state_desc
)
SELECT DB_NAME() AS DB_NAME
     , users.User_Type
     , users.Database_User_Name
     , users.Login_Name
     , users.DB_Role
     , users.is_fixed_role
     , o.[Schema]
     , o.Object
     , o.[User]
     , o.type
     , o.type_desc
     , o.permission_name
     , o.state_desc
FROM users
LEFT JOIN objects o ON o.[User] = users.Login_Name
 use SageEMSInfoServer
	
;WITH users
AS (
   
SELECT User_Type = CASE members.[type]
                       WHEN 'G' THEN
                           'Windows Group'
                       WHEN 'S' THEN
                           'SQL User'
                       WHEN 'U' THEN
                           'Windows User'
                   END
     , Database_User_Name = members.[name]
     , Login_Name = ul.[name]
     , roles.name AS DB_Role
     , roles.is_fixed_role
FROM sys.database_role_members AS database_role_members --mmbr
LEFT JOIN sys.database_principals AS roles -- rolp
    ON database_role_members.role_principal_id = roles.principal_id
LEFT JOIN sys.database_principals AS members -- mmbrp
    ON database_role_members.member_principal_id = members.principal_id
LEFT JOIN sys.server_principals ul ON ul.sid = members.sid
   )
    , objects
AS (
   SELECT s.name AS [Schema]
        , o.name AS Object
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   LEFT JOIN sys.objects o ON dp.major_id = o.object_id
   LEFT JOIN sys.schemas s ON o.schema_id = s.schema_id
   LEFT JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
  
   UNION ALL
   SELECT s.name AS [Schema]
        , NULL
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   JOIN sys.schemas s ON dp.major_id = s.schema_id
   JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
   )
INSERT ##users
(
    DB_NAME
  , User_Type
  , Database_User_Name
  , Login_Name
  , DB_Role
  , is_fixed_role
  , [Schema]
  , Object
  , [User]
  , type
  , type_desc
  , permission_name
  , state_desc
)
SELECT DB_NAME() AS DB_NAME
     , users.User_Type
     , users.Database_User_Name
     , users.Login_Name
     , users.DB_Role
     , users.is_fixed_role
     , o.[Schema]
     , o.Object
     , o.[User]
     , o.type
     , o.type_desc
     , o.permission_name
     , o.state_desc
FROM users
LEFT JOIN objects o ON o.[User] = users.Login_Name
 use SageEMSReporting
	
;WITH users
AS (
   
SELECT User_Type = CASE members.[type]
                       WHEN 'G' THEN
                           'Windows Group'
                       WHEN 'S' THEN
                           'SQL User'
                       WHEN 'U' THEN
                           'Windows User'
                   END
     , Database_User_Name = members.[name]
     , Login_Name = ul.[name]
     , roles.name AS DB_Role
     , roles.is_fixed_role
FROM sys.database_role_members AS database_role_members --mmbr
LEFT JOIN sys.database_principals AS roles -- rolp
    ON database_role_members.role_principal_id = roles.principal_id
LEFT JOIN sys.database_principals AS members -- mmbrp
    ON database_role_members.member_principal_id = members.principal_id
LEFT JOIN sys.server_principals ul ON ul.sid = members.sid
   )
    , objects
AS (
   SELECT s.name AS [Schema]
        , o.name AS Object
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   LEFT JOIN sys.objects o ON dp.major_id = o.object_id
   LEFT JOIN sys.schemas s ON o.schema_id = s.schema_id
   LEFT JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
  
   UNION ALL
   SELECT s.name AS [Schema]
        , NULL
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   JOIN sys.schemas s ON dp.major_id = s.schema_id
   JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
   )
INSERT ##users
(
    DB_NAME
  , User_Type
  , Database_User_Name
  , Login_Name
  , DB_Role
  , is_fixed_role
  , [Schema]
  , Object
  , [User]
  , type
  , type_desc
  , permission_name
  , state_desc
)
SELECT DB_NAME() AS DB_NAME
     , users.User_Type
     , users.Database_User_Name
     , users.Login_Name
     , users.DB_Role
     , users.is_fixed_role
     , o.[Schema]
     , o.Object
     , o.[User]
     , o.type
     , o.type_desc
     , o.permission_name
     , o.state_desc
FROM users
LEFT JOIN objects o ON o.[User] = users.Login_Name
 use SageEMSStandingData
	
;WITH users
AS (
   
SELECT User_Type = CASE members.[type]
                       WHEN 'G' THEN
                           'Windows Group'
                       WHEN 'S' THEN
                           'SQL User'
                       WHEN 'U' THEN
                           'Windows User'
                   END
     , Database_User_Name = members.[name]
     , Login_Name = ul.[name]
     , roles.name AS DB_Role
     , roles.is_fixed_role
FROM sys.database_role_members AS database_role_members --mmbr
LEFT JOIN sys.database_principals AS roles -- rolp
    ON database_role_members.role_principal_id = roles.principal_id
LEFT JOIN sys.database_principals AS members -- mmbrp
    ON database_role_members.member_principal_id = members.principal_id
LEFT JOIN sys.server_principals ul ON ul.sid = members.sid
   )
    , objects
AS (
   SELECT s.name AS [Schema]
        , o.name AS Object
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   LEFT JOIN sys.objects o ON dp.major_id = o.object_id
   LEFT JOIN sys.schemas s ON o.schema_id = s.schema_id
   LEFT JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
  
   UNION ALL
   SELECT s.name AS [Schema]
        , NULL
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   JOIN sys.schemas s ON dp.major_id = s.schema_id
   JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
   )
INSERT ##users
(
    DB_NAME
  , User_Type
  , Database_User_Name
  , Login_Name
  , DB_Role
  , is_fixed_role
  , [Schema]
  , Object
  , [User]
  , type
  , type_desc
  , permission_name
  , state_desc
)
SELECT DB_NAME() AS DB_NAME
     , users.User_Type
     , users.Database_User_Name
     , users.Login_Name
     , users.DB_Role
     , users.is_fixed_role
     , o.[Schema]
     , o.Object
     , o.[User]
     , o.type
     , o.type_desc
     , o.permission_name
     , o.state_desc
FROM users
LEFT JOIN objects o ON o.[User] = users.Login_Name
 use SimEnergyProd
	
;WITH users
AS (
   
SELECT User_Type = CASE members.[type]
                       WHEN 'G' THEN
                           'Windows Group'
                       WHEN 'S' THEN
                           'SQL User'
                       WHEN 'U' THEN
                           'Windows User'
                   END
     , Database_User_Name = members.[name]
     , Login_Name = ul.[name]
     , roles.name AS DB_Role
     , roles.is_fixed_role
FROM sys.database_role_members AS database_role_members --mmbr
LEFT JOIN sys.database_principals AS roles -- rolp
    ON database_role_members.role_principal_id = roles.principal_id
LEFT JOIN sys.database_principals AS members -- mmbrp
    ON database_role_members.member_principal_id = members.principal_id
LEFT JOIN sys.server_principals ul ON ul.sid = members.sid
   )
    , objects
AS (
   SELECT s.name AS [Schema]
        , o.name AS Object
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   LEFT JOIN sys.objects o ON dp.major_id = o.object_id
   LEFT JOIN sys.schemas s ON o.schema_id = s.schema_id
   LEFT JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
  
   UNION ALL
   SELECT s.name AS [Schema]
        , NULL
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   JOIN sys.schemas s ON dp.major_id = s.schema_id
   JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
   )
INSERT ##users
(
    DB_NAME
  , User_Type
  , Database_User_Name
  , Login_Name
  , DB_Role
  , is_fixed_role
  , [Schema]
  , Object
  , [User]
  , type
  , type_desc
  , permission_name
  , state_desc
)
SELECT DB_NAME() AS DB_NAME
     , users.User_Type
     , users.Database_User_Name
     , users.Login_Name
     , users.DB_Role
     , users.is_fixed_role
     , o.[Schema]
     , o.Object
     , o.[User]
     , o.type
     , o.type_desc
     , o.permission_name
     , o.state_desc
FROM users
LEFT JOIN objects o ON o.[User] = users.Login_Name
 use Trading
	
;WITH users
AS (
   
SELECT User_Type = CASE members.[type]
                       WHEN 'G' THEN
                           'Windows Group'
                       WHEN 'S' THEN
                           'SQL User'
                       WHEN 'U' THEN
                           'Windows User'
                   END
     , Database_User_Name = members.[name]
     , Login_Name = ul.[name]
     , roles.name AS DB_Role
     , roles.is_fixed_role
FROM sys.database_role_members AS database_role_members --mmbr
LEFT JOIN sys.database_principals AS roles -- rolp
    ON database_role_members.role_principal_id = roles.principal_id
LEFT JOIN sys.database_principals AS members -- mmbrp
    ON database_role_members.member_principal_id = members.principal_id
LEFT JOIN sys.server_principals ul ON ul.sid = members.sid
   )
    , objects
AS (
   SELECT s.name AS [Schema]
        , o.name AS Object
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   LEFT JOIN sys.objects o ON dp.major_id = o.object_id
   LEFT JOIN sys.schemas s ON o.schema_id = s.schema_id
   LEFT JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
  
   UNION ALL
   SELECT s.name AS [Schema]
        , NULL
        , u.name AS [User]
        , u.type
        , u.type_desc
        , dp.permission_name
        , dp.state_desc
   FROM sys.database_permissions dp
   JOIN sys.schemas s ON dp.major_id = s.schema_id
   JOIN sys.database_principals u ON dp.grantee_principal_id = u.principal_id
   )
INSERT ##users
(
    DB_NAME
  , User_Type
  , Database_User_Name
  , Login_Name
  , DB_Role
  , is_fixed_role
  , [Schema]
  , Object
  , [User]
  , type
  , type_desc
  , permission_name
  , state_desc
)
SELECT DB_NAME() AS DB_NAME
     , users.User_Type
     , users.Database_User_Name
     , users.Login_Name
     , users.DB_Role
     , users.is_fixed_role
     , o.[Schema]
     , o.Object
     , o.[User]
     , o.type
     , o.type_desc
     , o.permission_name
     , o.state_desc
FROM users
LEFT JOIN objects o ON o.[User] = users.Login_Name
 
SELECT *
FROM ##users ORDER by
DB_NAME
  , User_Type
  , Database_User_Name
  , Login_Name
  , DB_Role
  , is_fixed_role
  , [Schema]
  , Object
  , [User]
  , type
  , type_desc
  , permission_name
  , state_desc