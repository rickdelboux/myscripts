SELECT @@SERVERNAME
SELECT
sj.name 
,DATEDIFF(SECOND,aj.start_execution_date,GETDATE()) AS Seconds
,(DATEDIFF(SECOND,aj.start_execution_date,GETDATE()))/60 AS minutes 
,((DATEDIFF(SECOND,aj.start_execution_date,GETDATE()))/60) /60 AS Hours
FROM msdb..sysjobactivity aj
JOIN msdb..sysjobs sj ON sj.job_id = aj.job_id
WHERE aj.stop_execution_date IS NULL -- job hasn't stopped running
AND aj.start_execution_date IS NOT NULL -- job is currently running
--AND sj.name = 'JobX'
AND NOT EXISTS( -- make sure this is the most recent run
    select 1
    from msdb..sysjobactivity new
    where new.job_id = aj.job_id
    and new.start_execution_date > aj.start_execution_date
)
order BY DATEDIFF(SECOND,aj.start_execution_date,GetDate()) desc