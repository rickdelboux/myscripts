--****************************************************************************************
-- This script returns a (graphical) timeline for all SQL jobs using google graph
--****************************************************************************************
-- Version: 1.1
-- Author:	Theo Ekelmans
-- Email:	theo@ekelmans.com
-- Date:	2015-06-24
--****************************************************************************************
set nocount on 

declare @DT datetime 
DECLARE @StartDT DATETIME 
DECLARE @EndDT DATETIME 
DECLARE @MinRuntimeInSec INT
DECLARE @SendMail INT = 0
DECLARE @ReturnRecocordset INT = 1
DECLARE @Emailprofilename VARCHAR(50)
DECLARE @EmailRecipients VARCHAR(50)
-- chart 2 
DECLARE @StartDate DATETIME
    ,@Interval TINYINT  -- minutes
    ,@End INT
	,@TimeSlots int ;


--***************************************************************************************
-- Set variables
--***************************************************************************************
SET @StartDT ='2022-09-05 10:00:00.000' --DATEADD(dd, DATEDIFF(dd, 0, GETDATE()), -1)  -- midnight yesterday
SET @EndDT = '2022-09-05 13:00:00.000' --DATEADD(MINUTE,-1, DATEADD(dd, DATEDIFF(dd, 0, @StartDT ), 0)) -- 11:59
SET @MinRuntimeInSec = 1 --300 --Ignore jobs with runtime smaller then this
SET @StartDate = @StartDT
SET @Interval = 5  -- minutes
SET @TimeSlots = 1440/@Interval
SELECT @TimeSlots, @startdt, @enddt

/* Set this to match your @interval!  Neglecting it will make for incomplete
    results and/or very odd results.
	5 288
	10 144
    15 minute intervals = 96 records
    30 minute intervals = 48 records
    60 minute intervals = 24 records (can also use the Day script)
*/
SET @End = DATEPART(DY, DATEADD(DD, -1, DATEADD(YY, 1, @StartDate))) * @TimeSlots;





SET @ReturnRecocordset = 1 
SET @SendMail = 0
SET @Emailprofilename = '<ProfileName>'
SET @EmailRecipients = '<email>'

--***************************************************************************************
-- Pre-run cleanup (just in case)
--***************************************************************************************
IF OBJECT_ID('tempdb..#JobRuntime') IS NOT NULL DROP TABLE #JobRuntime;
IF OBJECT_ID('tempdb..##GoogleGraph') IS NOT NULL DROP TABLE ##GoogleGraph;
IF OBJECT_ID('tempdb..#Tally') IS NOT NULL DROP TABLE #Tally
IF OBJECT_ID('tempdb..#CalQuarterHour') IS NOT NULL DROP TABLE #CalQuarterHour
IF OBJECT_ID('tempdb..#JobsNoRunTimeline') IS NOT NULL DROP TABLE #JobsNoRunTimeline;


--***************************************************************************************
-- Create a table for HTML assembly
--***************************************************************************************
CREATE TABLE ##GoogleGraph ([ID] [INT] IDENTITY(1,1) NOT NULL,
							[HTML] [VARCHAR](8000) NULL)

--***************************************************************************************
-- Create the Job Runtime information table
--***************************************************************************************
SELECT	job.name AS JobName
		,cat.name AS CatName
		,CONVERT(DATETIME, CONVERT(CHAR(8), run_date, 112) + ' ' + STUFF(STUFF(RIGHT('000000' + CONVERT(VARCHAR(8), run_time), 6), 5, 0, ':'), 3, 0, ':'), 120) AS SDT
		,DATEADD(	s,
					((run_duration/10000)%100 * 3600) + ((run_duration/100)%100 * 60) + run_duration%100 ,
					CONVERT(DATETIME, CONVERT(CHAR(8), run_date, 112) + ' ' + STUFF(STUFF(RIGHT('000000' + CONVERT(VARCHAR(8), run_time), 6), 5, 0, ':'), 3, 0, ':'), 120) 
				) AS EDT
INTO	#JobRuntime
FROM	msdb.dbo.sysjobs job 
			LEFT JOIN msdb.dbo.sysjobhistory his
				ON his.job_id = job.job_id
			INNER JOIN msdb.dbo.syscategories cat
				ON job.category_id = cat.category_id
WHERE	CONVERT(DATETIME, CONVERT(CHAR(8), run_date, 112) + ' ' + STUFF(STUFF(RIGHT('000000' + CONVERT(VARCHAR(8), run_time), 6), 5, 0, ':'), 3, 0, ':'), 120) BETWEEN @StartDT AND @EndDT
AND		step_id = 0 -- step_id = 0 is the entire job, step_id > 0 is actual step number
AND		((run_duration/10000)%100 * 3600) + ((run_duration/100)%100 * 60) + run_duration%100 > @MinRuntimeInSec  -- Ignore trivial runtimes
ORDER BY SDT


CREATE TABLE #Tally (N INT, CONSTRAINT PK_Tally_N PRIMARY KEY CLUSTERED (N));

DECLARE 
    @Counter INT = 1;

WHILE @Counter <= @TimeSlots + 1 --- 10 minutes 1440 minutes per day
BEGIN
                 
    INSERT INTO #Tally (
        N
    )
    VALUES (
        @Counter
    );

    SET @Counter = @Counter + 1;
               
END;




CREATE TABLE #CalQuarterHour ([ID] [INT] IDENTITY(1,1) NOT NULL
, CalStartDt SMALLDATETIME
)

INSERT #CalQuarterHour
(
    CalStartDt
)
SELECT  
     DATEADD(MI, (N - 1) * @Interval, @StartDate) AS CalStartDt
 
FROM #Tally AS T
WHERE N < @End + 1
AND DATEADD(MI, (N - 1) * @Interval, @StartDate)  <= @EndDT;



SELECT ch.CalStartDt
,COUNT(jt.JobName) AS JobNo
INTO  #JobsNoRunTimeline
FROM #CalQuarterHour ch
LEFT JOIN #JobRuntime jt ON ch.CalStartDt BETWEEN jt.SDT AND jt.EDT
GROUP BY  ch.CalStartDt
ORDER BY ch.CalStartDt

IF NOT EXISTS (SELECT 1 FROM #JobRuntime) 
	GOTO NothingToDo

--***************************************************************************************
-- Format for google graph - Header 
-- (Split into multiple inserts because the default text result setting is 256 chars)
--***************************************************************************************
insert into ##GoogleGraph (HTML) 
select '<html>
	<head>
	<!--<META HTTP-EQUIV="refresh" CONTENT="3">-->
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>'
insert into ##GoogleGraph (HTML) 
select '    <script type="text/javascript">
	 google.charts.load(''current'', {''packages'':[''timeline'']});
      google.charts.setOnLoadCallback(drawChart);
	function drawChart() {'
insert into ##GoogleGraph (HTML) 
select '	var container = document.getElementById(''JobTimeline'');
	var chart = new google.visualization.Timeline(container);
	var dataTable = new google.visualization.DataTable();'
insert into ##GoogleGraph (HTML) 
select '	dataTable.addColumn({ type: ''string'', id: ''Position'' });
	dataTable.addColumn({ type: ''string'', id: ''Name'' });
	dataTable.addColumn({ type: ''date'', id: ''Start'' });
	dataTable.addColumn({ type: ''date'', id: ''End'' });
	dataTable.addRows([
'

--***************************************************************************************
-- Format for google graph - Data
--***************************************************************************************
insert into ##GoogleGraph (HTML) 
SELECT  '		[ ' 
		+'''' + CatName  + ''', '
		+'''' + JobName  + ''', '
		+'new Date('
		+     cast(DATEPART(year ,  SDT) as varchar(4))
		+', '+cast(DATEPART(month,  SDT) -1 as varchar(4)) --Java months count from 0
		+', '+cast(DATEPART(day,    SDT) as varchar(4))
		+', '+cast(DATEPART(hour,   SDT) as varchar(4))
		+', '+cast(DATEPART(minute, SDT) as varchar(4))
		+', '+cast(DATEPART(second, SDT) as varchar(4)) 
		+'), '

		+'new Date('
		+     cast(DATEPART(year,   EDT) as varchar(4))
		+', '+cast(DATEPART(month,  EDT) -1 as varchar(4)) --Java months count from 0
		+', '+cast(DATEPART(day,    EDT) as varchar(4))
		+', '+cast(DATEPART(hour,   EDT) as varchar(4))
		+', '+cast(DATEPART(minute, EDT) as varchar(4))
		+', '+cast(DATEPART(second, EDT) as varchar(4)) 
		+ ') ],' --+ char(10)
from	#JobRuntime 

--***************************************************************************************
-- Format for google graph - Footer
--***************************************************************************************
insert into ##GoogleGraph (HTML) 
select '	]);

	var options = 
	{
		timeline: 	{ 
					groupByRowLabel: true,
					colorByRowLabel: false,
					singleColor: false,
					rowLabelStyle: {fontName: ''Helvetica'', fontSize: 14 },
					barLabelStyle: {fontName: ''Helvetica'', fontSize: 14 }					
					}
	};

	 chart.draw(dataTable,options);

}'
insert into ##GoogleGraph (HTML) 
select '
	</script>
	</head>
	<body>'
	+'<font face="Helvetica" size="3" >'
	+'Job timeline on: '+@@servername
	+' from '+convert(varchar(20), @StartDT, 120)
	+' until '+convert(varchar(20), @EndDT, 120)
	+case when @MinRuntimeInSec = 0 then '' else ' (hiding jobs with runtime < '
	+cast(@MinRuntimeInSec as varchar(10))+' seconds)'
	END
	+'</font>
		<div id="JobTimeline" style="width: 1885px; height: 900px;"></div>
	</body>
</html>'


--*********************************CHART 2 ******************************************************
-- Format for google graph - Header 
-- (Split into multiple inserts because the default text result setting is 256 chars)
--***************************************************************************************
INSERT INTO ##GoogleGraph (HTML) 
SELECT '<html>
	<head>
	<!--<META HTTP-EQUIV="refresh" CONTENT="3">-->
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>'

-----------------------------------------------Chart 2 ------------------------------------------------------------------

INSERT INTO ##GoogleGraph (HTML) 
SELECT '    <script type="text/javascript">
	 google.charts.load(''current'', {''packages'':[''corechart'']});
      google.charts.setOnLoadCallback(drawChart);
	function drawChart() {'
INSERT INTO ##GoogleGraph (HTML) 
SELECT '	var container = document.getElementById(''NoofJobs'');
	var chart = new google.visualization.LineChart(container);
	var dataTable = new google.visualization.DataTable();'
INSERT INTO ##GoogleGraph (HTML) 

SELECT '	
	dataTable.addColumn({ type: ''datetime'', id: ''Start'' });
	dataTable.addColumn({ type: ''number'', id: ''NoJobs'' });
	dataTable.addRows([
'

--***************************************************************************************
-- Format for google graph - Data
--***************************************************************************************
INSERT INTO ##GoogleGraph (HTML) 
SELECT  '		[ ' 
		
		+'new Date('
		+     CAST(DATEPART(YEAR , j.CalStartDt) AS VARCHAR(4))
		+', '+CAST(DATEPART(MONTH, j.CalStartDt) -1 AS VARCHAR(4)) --Java months count from 0
		+', '+CAST(DATEPART(DAY,   j.CalStartDt) AS VARCHAR(4))
		+', '+CAST(DATEPART(HOUR,  j.CalStartDt) AS VARCHAR(4))
		+', '+CAST(DATEPART(MINUTE,j.CalStartDt) AS VARCHAR(4))
		+', '+CAST(DATEPART(SECOND,j.CalStartDt) AS VARCHAR(4)) 
		+'), '

		+ CAST(j.JobNo AS VARCHAR) 
		+ ' ],' --+ char(10)
FROM	#JobsNoRunTimeline j

--***************************************************************************************
-- Format for google graph - Footer
--***************************************************************************************
INSERT INTO ##GoogleGraph (HTML) 
SELECT '	]);

	var options = {
        hAxis: {
          title: ''Time in minute intervals = '  + CAST(@Interval AS VARCHAR(3)) +  ''' 
        },
        vAxis: {
          title: ''No of Jobs''
        },
		 legend: { position: ''bottom'' },
        backgroundColor: ''#f1f8e9'' 
		};

	 chart.draw(dataTable,options);

}'
insert into ##GoogleGraph (HTML) 
select '
	</script>
	</head>
	<body>'
	+'<font face="Helvetica" size="3" >'
	+'No of Jobs on: '+@@servername
	+' from '+convert(varchar(20), @StartDT, 120)
	+' until '+convert(varchar(20), @EndDT, 120)
	+case when @MinRuntimeInSec = 0 then '' else ' (hiding jobs with runtime < '+cast(@MinRuntimeInSec as varchar(10))+' seconds)' end
	+'</font>
		<div id="NoofJobs" style="width: 1885px; height: 900px;"></div>
	</body>
</html>'



--***************************************************************************************
-- Output HTML page - copy output & paste to a .HTML file and open with google chrome
--***************************************************************************************
if @ReturnRecocordset = 1 
	select html from ##GoogleGraph order by ID

--***************************************************************************************
-- Send Email - 
--***************************************************************************************
if @SendMail = 1 
	execute msdb.dbo.sp_send_dbmail	
		 @profile_name = @Emailprofilename
		,@recipients = @EmailRecipients
		,@subject = 'JobTimeline'
		,@body = 'See attachment for JobTimeline, open with Google Chrome!' 
		,@body_format = 'HTML' -- or TEXT
		,@importance = 'Normal' --Low Normal High
		,@sensitivity = 'Normal' --Normal Personal Private Confidential
		,@execute_query_database = 'master'
		,@query_result_header = 1
		,@query = 'set nocount on; SELECT HTML FROM ##GoogleGraph'
		,@query_result_no_padding = 1  -- prevent SQL adding padding spaces in the result
		--,@query_no_truncate= 1       -- mutually exclusive with @query_result_no_padding 
		,@query_no_truncate= 1
		,@query_attachment_filename= 'JobTimeline.HTML'

goto Cleanup

--***************************************************************************************
-- Just in case....
--***************************************************************************************
NothingToDo:

print 'No job runtime info found....'

--***************************************************************************************
-- Cleanup
--***************************************************************************************
Cleanup:
IF OBJECT_ID('tempdb..#JobRuntime') IS NOT NULL DROP TABLE #JobRuntime;
IF OBJECT_ID('tempdb..##GoogleGraph') IS NOT NULL DROP TABLE ##GoogleGraph;
