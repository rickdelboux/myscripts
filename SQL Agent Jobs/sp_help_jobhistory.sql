-- lists all job information for the NightlyBackups job.  
USE msdb ;  
GO  

EXEC dbo.sp_help_jobhistory   
    @job_name = N'ADMIN-Integrity Check_USER_[No SageEMS]_Saturday' ;  
GO  

EXEC dbo.sp_help_jobhistory   
    @job_name = N'ADMIN-Integrity Check_USER_[No SageEMS]_Monthly' ;  
GO  

EXEC dbo.sp_help_jobhistory   
    @job_name = N'ADMIN-Integrity Check SYSTEM' ;  
GO  


EXEC dbo.sp_help_jobhistory   
    @job_name = N'ADMIN-Integrity Check_SageEMS_Friday' ;  
GO  

EXEC dbo.sp_help_jobhistory   
    @job_name = N'ADMIN-Integrity Check_SageEMS_Monthly' ;  
GO  