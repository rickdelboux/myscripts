USE master
DECLARE @command1  varchar(2000) = N''    -- nvarchar(2000)

SET @command1  = @command1  + '
use ?
SELECT DB_NAME() AS DbName, 
    name AS FileName, 
	s.physical_name,
    type_desc,
    size/128.0 AS CurrentSizeMB,  
    size/128.0 - CAST(FILEPROPERTY(name, ''SpaceUsed'') AS INT)/128.0 AS FreeSpaceMB
FROM sys.database_files s
WHERE type IN (0,1);

'

EXEC sys.sp_MSforeachdb @command1
                    

SELECT DB_NAME() AS DbName, 
    name AS FileName, 
    type_desc,
    size/128.0 AS CurrentSizeMB,  
    size/128.0 - CAST(FILEPROPERTY(name, 'SpaceUsed') AS INT)/128.0 AS FreeSpaceMB
	,s.physical_name
FROM sys.database_files s
WHERE type IN (0,1);
