/*To catch long-running or uncommitted transactions, use another set of DMVs for viewing
current open transactions, including sys.dm_tran_database_transactions, sys.dm_tran_session_transactions, 
sys.dm_exec_connections, and sys.dm_exec_sql_text. 
There are several DMVs associated with tracking transactions, see more DMVs on transactions here.
https://docs.microsoft.com/en-us/sql/relational-databases/system-dynamic-management-views/transaction-related-dynamic-management-views-and-functions-transact-sql
*/

SELECT GETDATE()
SELECT [s_tst].[session_id],
[database_name] = DB_NAME (s_tdt.database_id),
[s_tdt].[database_transaction_begin_time], 
[sql_text] = [s_est].[text] 
FROM sys.dm_tran_database_transactions [s_tdt]
INNER JOIN sys.dm_tran_session_transactions [s_tst] ON [s_tst].[transaction_id] = [s_tdt].[transaction_id]
INNER JOIN sys.dm_exec_connections [s_ec] ON [s_ec].[session_id] = [s_tst].[session_id]
CROSS APPLY sys.dm_exec_sql_text ([s_ec].[most_recent_sql_handle]) AS [s_est]
where DB_NAME (s_tdt.database_id) = 'ERMPower_MSCRM';

--Identify tasks from blocked sessions.
SELECT * FROM sys.dm_os_waiting_tasks 
WHERE blocking_session_id IS NOT NULL; 

-- this view returns information about the currently active lock manager resources in SQL Server.
SELECT *
FROM sys.dm_tran_locks
WHERE resource_database_id = (SELECT db.database_id
FROM sys.databases AS db
WHERE name = 'ERMPower_MSCRM');