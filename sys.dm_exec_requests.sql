
--WAITS
SELECT session_id, blocking_session_id, start_time, wait_type, wait_type
FROM sys.dm_exec_requests
WHERE blocking_session_id > 0;		

SELECT DISTINCT DEC.session_id, DST.text AS SQL
FROM sys.dm_exec_requests AS DER
  JOIN sys.dm_exec_connections AS DEC
    ON DER.blocking_session_id = DEC.session_id
  CROSS APPLY sys.dm_exec_sql_text(DEC.most_recent_sql_handle) AS DST;		

  SELECT session_id, open_transaction_count
FROM sys.dm_exec_sessions
WHERE open_transaction_count > 0;	

-- Active SQL Server Plan
SELECT DER.session_id, DEQP.query_plan 
FROM sys.dm_exec_requests AS DER
  CROSS APPLY sys.dm_exec_query_plan(DER.plan_handle) AS DEQP
WHERE NOT DER.status IN ('background', 'sleeping');	

--All Active SQL Server Requests to a Particular Database
SELECT DER.session_id, DES.login_name, DES.program_name
FROM sys.dm_exec_requests AS DER
  JOIN sys.databases AS DB
    ON DER.database_id = DB.database_id
  JOIN sys.dm_exec_sessions AS DES
    ON DER.session_id = DES.session_id
WHERE DB.name = 'Test';		

--Count of All Active SQL Server Wait Types
SELECT COALESCE(wait_type, 'None') AS wait_type, COUNT(*) AS Total
FROM sys.dm_exec_requests
WHERE NOT status IN ('Background', 'Sleeping')
GROUP BY wait_type 
ORDER BY Total DESC;	

--We can then query along with sys.dm_tran_locks to determine what types of locks these active requests were trying to obtain:

SELECT L.request_session_id, L.resource_type, 
  L.resource_subtype, L.request_mode, L.request_type 
FROM sys.dm_tran_locks AS L
  JOIN sys.dm_exec_requests AS DER
    ON L.request_session_id = DER.session_id
WHERE DER.wait_type = 'LCK_M_S';		