USE SageEMS;



SELECT User_Type = CASE mmbrp.[type]
                       WHEN 'G' THEN
                           'Windows Group'
                       WHEN 'S' THEN
                           'SQL User'
                       WHEN 'U' THEN
                           'Windows User'
                   END,
       Database_User_Name = mmbrp.[name],
       Login_Name = ul.[name],
       DB_Role = rolp.[name]
	   ,rolp.is_fixed_role
	 
FROM sys.database_role_members mmbr, -- The Role OR members associations table
     sys.database_principals rolp,   -- The DB Roles names table
     sys.database_principals mmbrp,  -- The Role members table (database users)
     sys.server_principals ul        -- The Login accounts table
WHERE UPPER(mmbrp.[type]) IN ('G','U','G' )  --S = sql user, U = Windows User , G = Windows Group
      -- No need for these system account types
      AND UPPER(mmbrp.[name]) NOT IN ( 'SYS', 'INFORMATION_SCHEMA' )
      AND rolp.[principal_id] = mmbr.[role_principal_id]
      AND mmbrp.[principal_id] = mmbr.[member_principal_id]
      AND ul.[sid] = mmbrp.[sid]
      --AND rolp.[name] LIKE '%' + @dbRole + '%'
     
ORDER BY Login_Name


SELECT s.name AS [Schema],
       o.name AS Object,
       u.name AS [User],
	   u.type,
	   u.type_desc,
       dp.permission_name,
       dp.state_desc
FROM sys.database_permissions dp
left   JOIN sys.objects o
        ON dp.major_id = o.object_id
left    JOIN sys.schemas s
        ON o.schema_id = s.schema_id
left    JOIN sys.database_principals u
        ON dp.grantee_principal_id = u.principal_id
--WHERE  u.name  = 'ERM\sg_it_dev'
UNION ALL
SELECT s.name AS [Schema],
       NULL,
       u.name AS [User],
	      u.type,
		    u.type_desc,
       dp.permission_name,
       dp.state_desc
FROM sys.database_permissions dp
    JOIN sys.schemas s
        ON dp.major_id = s.schema_id
    JOIN sys.database_principals u
        ON dp.grantee_principal_id = u.principal_id
--WHERE  u.name  = 'ERM\sg_it_dev'
ORDER BY 
--s.name,
--         o.name,
         u.name;

