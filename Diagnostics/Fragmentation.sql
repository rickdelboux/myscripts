USE SageEMS
SELECT Coalesce(Object_Schema_Name(indexes.object_id) + '.', '')
       + Coalesce(Object_Name(indexes.object_id) + '/', '')
       + Coalesce(indexes.name, 'Heap'), 
  Str(avg_fragmentation_in_percent, 10,1) AS [avg_fragmentation_%],
  Str(avg_page_space_used_in_percent, 10,1) AS [avg_page_space_used_%],
  fill_factor,
  Str((avg_record_size_in_bytes * record_count) / (1024.0 * 1024), 10,2) AS [IndexSize_(MB)]
  FROM sys.dm_db_index_physical_stats(Db_Id(), NULL, NULL, NULL, 'Sampled') AS IPS
    INNER JOIN sys.indexes
      ON indexes.index_id = IPS.index_id AND indexes.object_id = IPS.object_id
  WHERE ObjectProperty(indexes.object_id, 'IsUserTable') = 1 AND index_level=0 --leaf level
 
 -- ORDER BY [IndexSize_(MB)] DESC;


 -- SELECT Coalesce(Object_Schema_Name(indexes.object_id) + '.', '')
 --      + Coalesce(Object_Name(indexes.object_id) + '/', '')
 --      + Coalesce(indexes.name, 'Heap')
	--   , Str(avg_fragmentation_in_percent, 10,1) AS [avg_fragmentation_%],
 -- Str(avg_page_space_used_in_percent, 10,1) AS [avg_page_space_used_%],
 -- fill_factor,
 -- Str((avg_record_size_in_bytes * record_count) / (1024.0 * 1024), 10,2) AS [IndexSize_(MB)]
 -- FROM  sys.indexes 
 --INNER JOIN  sys.dm_db_index_physical_stats(Db_Id(), NULL, NULL, NULL, 'Sampled') AS IPS  ON indexes.index_id = IPS.index_id AND indexes.object_id = IPS.object_id
 -- WHERE Object_Name(indexes.object_id) = 'tbSiteIntervalData'