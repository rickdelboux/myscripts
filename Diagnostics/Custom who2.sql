DECLARE @Who TABLE
(
    SPID INT NOT NULL,
    [Status] VARCHAR(1000) NULL,
    [Login] sysname NULL,
    HostName sysname NULL,
    BlkBy sysname NULL,
    DBName sysname NULL,
    Command VARCHAR(1000) NULL,
    CPUTime INT NULL,
    DiskIO BIGINT NULL,
    LastBatch VARCHAR(1000) NULL,
    ProgramName VARCHAR(1000) NULL,
    SPID2 INT,
    RequestId INT NULL
);
INSERT INTO @Who
EXECUTE sp_who2;
SELECT w.*,
       r.total_elapsed_time / (1000.0) [Elapsed Time (s)],
       st.[text] [Request Text],
       SUBSTRING(   st.[text],
                    (r.statement_start_offset / 2) + 1,
                    ((CASE r.statement_end_offset
                          WHEN -1 THEN
                              DATALENGTH(st.[text])
                          ELSE
                              r.statement_end_offset
                      END - r.statement_start_offset
                     ) / 2
                    ) + 1
                ) [Statement Text],
       COALESCE(
                   QUOTENAME(DB_NAME(st.dbid)) + N'.' + QUOTENAME(OBJECT_SCHEMA_NAME(st.objectid, st.dbid)) + N'.'
                   + QUOTENAME(OBJECT_NAME(st.objectid, st.dbid)),
                   ''
               ) [Command Text],
       r.plan_handle
FROM @Who w
    JOIN sys.dm_exec_sessions s
        ON s.session_id = w.SPID
    JOIN sys.dm_exec_requests AS r
        ON r.session_id = s.session_id
    CROSS APPLY sys.dm_exec_sql_text(r.sql_handle) st;