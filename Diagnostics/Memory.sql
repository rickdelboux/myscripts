
---https://dba.stackexchange.com/questions/96188/interpreting-sys-dm-os-sys-memory
SELECT @@SERVERNAME AS ServerName
,'Page File' AS MemoryType
    ,CONVERT(DECIMAL(10, 2), 1.0 * total_page_file_kb / 1024) AS Total_MB
    ,CONVERT(DECIMAL(10, 2), 1.0 * available_page_file_kb / 1024) AS Free_MB
    ,CONVERT(VARCHAR(5), CONVERT(DECIMAL(10, 1),
        100.0 * available_page_file_kb / total_page_file_kb)) + '%' AS Free
FROM sys.dm_os_sys_memory
UNION ALL
SELECT  @@SERVERNAME AS ServerName
,'Physical (RAM)'
    ,CONVERT(DECIMAL(10, 2), 1.0 * total_physical_memory_kb / 1024)
    ,CONVERT(DECIMAL(10, 2), 1.0 * available_physical_memory_kb / 1024)
    ,CONVERT(VARCHAR(5), CONVERT(DECIMAL(10, 1),
        100.0 * available_physical_memory_kb / total_physical_memory_kb)) + '%'
FROM sys.dm_os_sys_memory
UNION ALL
SELECT @@SERVERNAME AS ServerName
,'Non-Physical'
    ,CONVERT(DECIMAL(10, 2),
         1.0 * (total_page_file_kb - total_physical_memory_kb) / 1024)
    ,CONVERT(DECIMAL(10, 2),
         1.0 * (available_page_file_kb - available_physical_memory_kb) / 1024)
    ,CONVERT(VARCHAR(5), CONVERT(DECIMAL(10, 1),
       100.0 * (available_page_file_kb - available_physical_memory_kb)
             / (total_page_file_kb - total_physical_memory_kb))) + '%'
FROM sys.dm_os_sys_memory
UNION ALL
SELECT  @@SERVERNAME AS ServerName
,'System Cache',CONVERT(DECIMAL(10, 2), 1.0 * system_cache_kb / 1024),NULL,NULL
FROM sys.dm_os_sys_memory
UNION ALL
SELECT  @@SERVERNAME AS ServerName
,'Kernel: Paged'
    ,CONVERT(DECIMAL(10, 2), 1.0 * kernel_paged_pool_kb / 1024),NULL,NULL
FROM sys.dm_os_sys_memory
UNION ALL
SELECT  @@SERVERNAME AS ServerName
,'Kernel: Non-Paged'
    ,CONVERT(DECIMAL(10, 2), 1.0 * kernel_nonpaged_pool_kb / 1024),NULL,NULL
FROM sys.dm_os_sys_memory




SELECT @@SERVERNAME AS ServerName
,getdate() AS CollectedDate
, total_physical_memory_kb
     , available_physical_memory_kb
     , total_page_file_kb
     , available_page_file_kb
     , system_cache_kb
     , kernel_paged_pool_kb
     , kernel_nonpaged_pool_kb
     , system_high_memory_signal_state
     , system_low_memory_signal_state
     --, system_memory_state_desc
FROM sys.dm_os_sys_memory


SELECT @@SERVERNAME AS ServerName
,getdate() AS CollectedDate
, total_physical_memory_kb
     , available_physical_memory_kb
     , total_page_file_kb
     , available_page_file_kb
     , system_cache_kb
     , kernel_paged_pool_kb
     , kernel_nonpaged_pool_kb
     , system_high_memory_signal_state
     , system_low_memory_signal_state
     --, system_memory_state_desc
FROM sys.dm_os_sys_memory

-- avaiable mem 8 gig
SELECT (9171720/1024)/1024


USE master
--Memory - Available Physical
SELECT available_physical_memory_kb
FROM sys.dm_os_sys_memory

--Memory -  Total Page File
SELECT total_page_file_kb
FROM sys.dm_os_sys_memory

--Memory - Available Page File
SELECT  available_page_file_kb FROM sys.dm_os_sys_memory

--Memory - System Cache
SELECT  system_cache_kb FROM sys.dm_os_sys_memory

--Memory - Kernel Paged Pool
SELECT  kernel_paged_pool_kb FROM sys.dm_os_sys_memory

--Memory - Kernel nonPaged Pool
SELECT  kernel_nonpaged_pool_kb FROM sys.dm_os_sys_memory

--Memory - System High Memory Signal State
SELECT  system_high_memory_signal_state FROM sys.dm_os_sys_memory
--Memory - System Low Memory Signal State
SELECT  system_low_memory_signal_state FROM sys.dm_os_sys_memory


--Memory - Page File % Free
SELECT 
'Page File' AS MemoryType

    ,CONVERT(DECIMAL(10, 1),
        100.0 * available_page_file_kb / total_page_file_kb)
		
FROM sys.dm_os_sys_memory

--Memory - Physical (RAM) % Free
SELECT 'Physical (RAM)'
   
	,CONVERT(DECIMAL(10, 1),
        100.0 * available_physical_memory_kb / total_physical_memory_kb)
FROM sys.dm_os_sys_memory

--Memory - Non-Physical % Free
SELECT @@SERVERNAME AS ServerName
	,'Non-Physical'
    
    ,CONVERT(DECIMAL(10, 1),
       100.0 * (available_page_file_kb - available_physical_memory_kb)
             / (total_page_file_kb - total_physical_memory_kb))
FROM sys.dm_os_sys_memory


--Memory - System Cache
SELECT 
'System Cache'
,CONVERT(DECIMAL(10, 2), 1.0 * system_cache_kb / 1024)
FROM sys.dm_os_sys_memory

--Memory - Kernel: Paged
SELECT  @@SERVERNAME AS ServerName
,'Kernel: Paged'
    ,CONVERT(DECIMAL(10, 2), 1.0 * kernel_paged_pool_kb / 1024)
FROM sys.dm_os_sys_memory

--Memory - Kernel: Non-Paged (Mb)
SELECT  @@SERVERNAME AS ServerName
,'Kernel: Non-Paged'
    ,CONVERT(DECIMAL(10, 2), 1.0 * kernel_nonpaged_pool_kb / 1024)
FROM sys.dm_os_sys_memory


SELECT total_page_file_kb, available_page_file_kb, 
system_memory_state_desc
FROM sys.dm_os_sys_memory

SELECT  
(physical_memory_in_use_kb/1024) AS Memory_usedby_Sqlserver_MB,  
(locked_page_allocations_kb/1024) AS Locked_pages_used_Sqlserver_MB,  
(total_virtual_address_space_kb/1024) AS Total_VAS_in_MB,  
process_physical_memory_low,  
process_virtual_memory_low  
FROM sys.dm_os_process_memory; 

DECLARE @counter BIGINT = 0

SET @counter = (
SELECT cntr_value
FROM sys.dm_os_performance_counters
WHERE [object_name] LIKE '%Buffer Manager%'
AND [counter_name] = 'Page writes/sec'
)

WAITFOR DELAY '00:00:10';

SELECT  (cntr_value - @counter) /10 AS 'Page writes/sec'
FROM sys.dm_os_performance_counters
WHERE [object_name] LIKE '%Buffer Manager%'
AND [counter_name] = 'Page writes/sec'

SELECT object_name, counter_name, cntr_value
FROM sys.dm_os_performance_counters
WHERE [object_name] LIKE '%Buffer Manager%'
AND [counter_name] = 'Buffer cache hit ratio'
 