SELECT message_id AS Error,
    severity AS Severity,
    [Event Logged] = CASE is_event_logged
        WHEN 0 THEN 'No' ELSE 'Yes'
        END,
    [text] AS [Description]
FROM sys.messages
WHERE language_id = 1033 /* replace 1040 with the desired language ID, such as 1033 for US English*/
AND message_id = 1474
ORDER BY message_id;