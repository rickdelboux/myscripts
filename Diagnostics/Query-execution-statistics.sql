 -- Get I/O utilization by database (Query 31) (IO Usage By Database)
  WITH Aggregate_IO_Statistics
  AS
  (SELECT DB_NAME(database_id) AS [Database Name],
  CAST(SUM(num_of_bytes_read + num_of_bytes_written)/1048576 AS DECIMAL(12, 2)) AS io_in_mb
  FROM sys.dm_io_virtual_file_stats(NULL, NULL) AS [DM_IO_STATS]
  GROUP BY database_id)
  SELECT ROW_NUMBER() OVER(ORDER BY io_in_mb DESC) AS [I/O Rank], [Database Name], io_in_mb AS [Total I/O (MB)],
         CAST(io_in_mb/ SUM(io_in_mb) OVER() * 100.0 AS DECIMAL(5,2)) AS [I/O Percent]
  FROM Aggregate_IO_Statistics
  ORDER BY [I/O Rank] OPTION (RECOMPILE);
   
  -- Helps determine which database is using the most I/O resources on the instance

  SELECT TOP 10
	        t.text ,
	        execution_count ,
	        statement_start_offset AS stmt_start_offset ,
	        sql_handle ,
	        plan_handle ,
	        total_logical_reads / execution_count AS avg_logical_reads ,
	        total_logical_writes / execution_count AS avg_logical_writes ,
	        total_physical_reads / execution_count AS avg_physical_reads
	FROM	sys.dm_exec_query_stats AS s
	        CROSS APPLY sys.dm_exec_sql_text(s.sql_handle) AS t
	WHERE	DB_NAME(t.dbid) = 'ERMPower_MSCRM'
	ORDER BY avg_physical_reads DESC;