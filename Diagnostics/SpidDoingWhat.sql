SELECT
(SELECT [text]
FROM sys.dm_exec_sql_text(sql_handle)
) AS SqlCommand,
spid AS [Process ID], status AS [Status],
hostname AS [Host Name], hostprocess AS [Host Process], SPACE(3) AS [Company],
0 AS [Task], SPACE(64) AS [Description],
loginame AS [User], open_tran AS [Open Trans], cmd AS [Command],
blocked AS [Blocked], CONVERT(VARCHAR(19), waittime) AS [Wait Time],
[Waiting] =
Case waittype
WHEN 0x0000 THEN SPACE(256)
Else waitresource
END, login_time AS [Login Time],
SPACE(64) AS [WTS Client], SPACE(12) AS [WTS ID],
program_name AS [Application]
FROM sys.sysprocesses WITH (NOLOCK)
WHERE
spid IN(181)

--pnsql26
--dd hh:mm:ss.mss	session_id
--00 00:01:11.830	181
--begin transaction  [4a0cd0c181624c8dbddbe8f4f4ba69d9]; 
--UPDATE [dbo].[awsdms_truncation_safeguard] set [latchLocker] = GETDATE()   
--WHERE [latchTaskName]='[4a0cd0c181624c8dbddbe8f4f4ba69d9]' and [latchMachineGUID]='4a0cd0c1-8162-4c8d-bddb-e8f4f4ba69d9' 
--AND [LatchKey]='B'

--begin transaction  [4a0cd0c181624c8dbddbe8f4f4ba69d9]; 
--UPDATE [dbo].[awsdms_truncation_safeguard] set [latchLocker] = GETDATE()   
--WHERE [latchTaskName]='[4a0cd0c181624c8dbddbe8f4f4ba69d9]' and [latchMachineGUID]='4a0cd0c1-8162-4c8d-bddb-e8f4f4ba69d9'
--AND [LatchKey]='B'
--dd hh:mm:ss.mss
--00 00:00:16.570
--begin transaction  [4a0cd0c181624c8dbddbe8f4f4ba69d9]; 
--UPDATE [dbo].[awsdms_truncation_safeguard] set [latchLocker] = GETDATE()  
--WHERE [latchTaskName]='[4a0cd0c181624c8dbddbe8f4f4ba69d9]' and [latchMachineGUID]='4a0cd0c1-8162-4c8d-bddb-e8f4f4ba69d9' 
--AND [LatchKey]='B'