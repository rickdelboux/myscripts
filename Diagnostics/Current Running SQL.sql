--https://www.mssqltips.com/sqlservertip/1811/find-current-running-sql-statement-in-sql-server/

SELECT @@SPID;

DECLARE @EndTime DATETIME = GETDATE()


SELECT SDER.session_id
	 , SDER.start_time
	 ,CASE DATEDIFF(HOUR, SDER.start_time, @EndTime)
      WHEN 0 THEN CAST(DATEDIFF(MINUTE, SDER.start_time, @EndTime) AS VARCHAR(10))  + ' mins'
      ELSE 
	  CAST(DATEDIFF(HOUR, SDER.start_time, @EndTime) AS VARCHAR(10)) + ' Hours'
		END AS Duration
	   , SDER.status
     , DB_NAME(s.dbid) AS DB
     , s.hostname
     , s.program_name
     , CASE
           WHEN CHARINDEX('TSQL JobStep', s.program_name) > 0 THEN
           (
               SELECT b.name
               FROM sys.dm_exec_sessions a
                  , msdb.dbo.sysjobs b
               WHERE a.session_id = SDER.session_id
                     AND (SUBSTRING(master.dbo.fn_varbintohexstr(CONVERT(VARBINARY(16), b.job_id)), 1, 10)) = SUBSTRING( a.program_name, 30, 10)                                                                                                                                                                                                                                                                                                                                                                     
           )
           ELSE
               ''
       END AS JobName
     , s.loginame

     --  , SDER.request_id 
    
     --  , SDER.row_count
   
     , SDER.command
     --, USER_NAME(SDER.user_id) AS UserName
     --, SUSER_NAME(SDER.user_id) AS [login name ]

     --, SDER.connection_id
     , SDER.blocking_session_id
     --, SDER.wait_type
     --, SDER.wait_time
     , SDER.percent_complete
     --, SDER.estimated_completion_time
     --, SDER.reads
     --, SDER.writes
     --, SDER.logical_reads
     --, SDER.lock_timeout
     --, SDER.deadlock_priority
     --, SDER.[statement_start_offset]
     --, SDER.[statement_end_offset]
     , CASE
           WHEN SDER.[statement_start_offset] > 0 THEN
               --The start of the active command is not at the beginning of the full command text 
               CASE SDER.[statement_end_offset]
                   WHEN -1 THEN
                       --The end of the full command is also the end of the active statement 
                       SUBSTRING(DEST.text, (SDER.[statement_start_offset] / 2) + 1, 2147483647)
                   ELSE
                       --The end of the active statement is not at the end of the full command 
                       SUBSTRING(
                                    DEST.text
                                  , (SDER.[statement_start_offset] / 2) + 1
                                  , (SDER.[statement_end_offset] - SDER.[statement_start_offset]) / 2
                                )
               END
           ELSE
               --1st part of full command is running 
               CASE SDER.[statement_end_offset]
                   WHEN -1 THEN
                       --The end of the full command is also the end of the active statement 
                       RTRIM(LTRIM(DEST.[text]))
                   ELSE
                       --The end of the active statement is not at the end of the full command 
                       LEFT(DEST.text, (SDER.[statement_end_offset] / 2) + 1)
               END
       END AS [executing statement]
     , DEST.[text] AS [full statement code]
FROM sys.[dm_exec_requests] SDER
CROSS APPLY sys.[dm_exec_sql_text](SDER.[sql_handle]) DEST
LEFT JOIN
(
    SELECT hostname
         , program_name
         , loginame
         , spid
         , s.dbid
    FROM sys.sysprocesses s
--WHERE s.spid = SDER.session_id
) s ON s.spid = SDER.session_id
WHERE SDER.session_id > 50
ORDER BY DATEDIFF(HOUR, SDER.start_time, @EndTime) desc

--ORDER BY SDER.[session_id]
--       , SDER.[request_id];