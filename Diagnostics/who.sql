DECLARE  
    @Who TABLE (  
        SPID INT NOT NULL,  
        [Status] VARCHAR(1000) NULL,  
        [Login] SYSNAME NULL,  
        HostName SYSNAME NULL,  
        BlkBy SYSNAME NULL,  
        DBName SYSNAME NULL,  
        Command VARCHAR(1000) NULL,  
        CPUTime INT NULL,  
        DiskIO BIGINT NULL,  
        LastBatch VARCHAR(1000) NULL,  
        ProgramName VARCHAR(1000) NULL,  
        SPID2 INT,  
        RequestId INT NULL  
    );  

INSERT INTO @Who  

EXECUTE sp_who2;  

SELECT 
    w.*,
    DATEDIFF(MILLISECOND, atr.transaction_begin_time, SYSDATETIME()) / 1000.0 [Transaction Duration (s)],
    r.total_elapsed_time / (1000.0) [Elapsed Time (s)], 
    ib.event_info [Input Buffer],
    st.[text] [Request Text], 
    SUBSTRING( 
    st.[text], 
    (r.statement_start_offset / 2) + 1,  
        ((CASE r.statement_end_offset  
            WHEN -1 THEN Datalength(st.[text])  
            ELSE r.statement_end_offset  
        END - r.statement_start_offset) / 2) + 1) [Statement Text],  
    COALESCE( 
        QUOTENAME(DB_NAME(st.dbid)) + N'.' + QUOTENAME(OBJECT_SCHEMA_NAME(st.objectid,st.dbid)) + N'.' + QUOTENAME(OBJECT_NAME(st.objectid,st.dbid)),  
        '' 
    ) [Command Text],
    atr.transaction_id [Transaction Id],
    CAST(qp.query_plan AS XML) [Query Plan]
FROM 
    @Who w 
    JOIN sys.dm_exec_sessions s ON s.session_id = w.SPID 
    LEFT JOIN sys.dm_exec_requests AS r ON r.session_id = s.session_id 
    OUTER APPLY sys.dm_exec_sql_text(r.sql_handle) st
    OUTER APPLY sys.dm_exec_text_query_plan (r.plan_handle, r.statement_start_offset, r.statement_end_offset) qp
    LEFT JOIN sys.dm_tran_session_transactions str ON str.session_id = s.session_id
    LEFT JOIN sys.dm_tran_active_transactions atr ON atr.transaction_id = str.transaction_id
    OUTER APPLY sys.dm_exec_input_buffer (s.session_id, NULL) ib
ORDER BY
    DATEDIFF(MILLISECOND, atr.transaction_begin_time, SYSDATETIME()) / 1000.0 DESC;