SELECT  OBJECT_Schema_NAME(keys.parent_object_id)+'.'+OBJECT_NAME(keys.parent_object_id) AS TheTable,
          keys.name AS Foreign_Key,
          STRING_AGG(COL_NAME(keys.parent_object_id,TheColumns.constraint_column_id),',') AS Column_List
          FROM sys.foreign_keys AS keys
            INNER JOIN sys.foreign_key_columns AS TheColumns
              ON keys.object_id = constraint_object_id
            LEFT OUTER JOIN sys.index_columns AS ic
              ON ic.object_id = TheColumns.parent_object_id
             AND ic.column_id = TheColumns.parent_column_id
             AND TheColumns.constraint_column_id = ic.key_ordinal
          WHERE ic.object_id IS NULL
GROUP BY keys.parent_object_id,keys.name
