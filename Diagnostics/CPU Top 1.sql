DECLARE @aDate DATETIME = '2020-01-01 00:00';

SELECT --a.ForecastId,
    f.Forecast
  , f.ForecastType
  , f.DefinedBy
  , s.OptionDescription AS STEPOption
  , a.SchemeId
  , DATEPART(ISO_WEEK, a.PeriodDateTime) AS PeriodWeek
  , DATEPART(MONTH, a.PeriodDateTime) AS PeriodMonth
  , DATEPART(YEAR, a.PeriodDateTime) AS PeriodYear
  --CAST(PeriodDateTime AS DATE) PeriodDate,
  , MIN(a.PeriodDateTime) FromDateTime
  , MAX(a.PeriodDateTime) ToDateTime
  , COUNT(*) AS ROW_COUNT
  , SUM(a.ActualLoad) ActualLoad
  , SUM(a.TransferCost) TransferCost
  , SUM(a.SalesTotal) SalesTotal
  , GETDATE() AS RunDate
FROM dbo.tbForecast f
INNER JOIN dbo.tbForecast_Actual_Period_Green a ON f.ForecastId = a.ForecastId
LEFT JOIN dbo.tbForecast_Dim_STEPOption s ON f.STEPOptionId = s.OptionId
WHERE 1 = 1
      AND a.PeriodDateTime >= @aDate --'2020-01-01 00:00'
      AND f.ForecastType IN ( 'A', 'B', 'N', 'P', 'K', 'L', 'R', 'E', 'S', 'T', 'U', 'Y', 'F', 'G', 'V' )
GROUP BY a.ForecastId
       , f.Forecast
       , f.ForecastType
       , f.DefinedBy
       , s.OptionDescription
       , a.SchemeId
       , DATEPART(ISO_WEEK, a.PeriodDateTime)
       , DATEPART(MONTH, a.PeriodDateTime)
       , DATEPART(YEAR, a.PeriodDateTime);