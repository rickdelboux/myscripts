--DECLARE @plan_handle varbinary(64) = 0x06000900B1FB342D60483FC22001000001000000000000000000000000000000000000000000000000000000
--DBCC FREEPROCCACHE (@plan_handle);  

SELECT decp.plan_handle,
dm_exec_sql_text.text AS TSQL_Text,
decp.usecounts 
FROM sys.dm_exec_cached_plans AS decp
JOIN sys.dm_exec_query_stats AS deqs
    ON decp.plan_handle = deqs.plan_handle
CROSS APPLY sys.dm_exec_sql_text(decp.plan_handle)
WHERE 1 = 1 
---AND decp.usecounts = 1
--AND   decp.objtype = 'Adhoc'
--AND   deqs.last_execution_time < DATEADD(HOUR, -1, GETDATE())
AND dm_exec_sql_text.text LIKE 'Select * from [billing].[vwContract]%'

--OPEN db_cursor  
--FETCH NEXT FROM db_cursor INTO @plan_handle  

--WHILE @@FETCH_STATUS = 0  
--BEGIN  
--    --DBCC FREEPROCCACHE (@plan_handle);  
--    SELECT @plan_handle
--	FETCH NEXT FROM db_cursor INTO @plan_handle 
--END 

--CLOSE db_cursor  
--DEALLOCATE db_cursor