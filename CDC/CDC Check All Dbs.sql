SET NOCOUNT ON;

---https://dba.stackexchange.com/questions/282632/how-does-aws-dms-on-going-replication-works-internally
 
DECLARE @cdcInfo TABLE (DatabaseName sysname, MinLSNTime datetime2, MaxLSNTime datetime2);
 
DECLARE @command varchar(1000);

SELECT @command = 'IF ''?'' NOT IN(''master'', ''model'', ''msdb'', ''tempdb'') BEGIN USE ?

IF (select is_cdc_enabled from sys.databases where [name] = ''?'') = 1 BEGIN

       DECLARE @cdcList TABLE (TableName NVARCHAR(128) PRIMARY KEY CLUSTERED, MinLSNTime datetime2);

       DECLARE @cdcTable NVARCHAR(128);

       INSERT @cdcList (TableName, MinLSNTime)
       SELECT
              capture_instance
              , sys.fn_cdc_map_lsn_to_time(sys.fn_cdc_get_min_lsn(capture_instance))
       FROM cdc.change_tables;

       SELECT
       DatabaseName = DB_NAME()
        , MinLSNTime = (SELECT MIN(MinLSNTime) FROM @cdcList WHERE MinLSNTime IS NOT NULL)
       , MaxLSNTime = sys.fn_cdc_map_lsn_to_time(sys.fn_cdc_get_max_lsn())
 
        END;
END' ;

INSERT @cdcInfo (DatabaseName, MinLSNTime, MaxLSNTime)
EXECUTE master.sys.sp_MSforeachdb @command;

SELECT DatabaseName, MinLSNTime, MaxLSNTime
FROM @cdcInfo
ORDER BY DatabaseName;