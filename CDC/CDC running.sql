
USE ERMPower_MSCRM
GO
SELECT [name], is_tracked_by_cdc 
FROM sys.tables
WHERE is_tracked_by_cdc  = 1
GO

USE msdb ;  
GO  
EXEC dbo.sp_help_job   @job_name = N'cdc.ERMPower_MSCRM_capture', @job_aspect='Job'	
GO  