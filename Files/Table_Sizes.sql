
USE Simenergyprod
;WITH Tsize AS (
SELECT GETDATE() AS date, SCHEMA_NAME(tab.schema_id) + '.' + tab.name AS [table], 
    CAST(SUM(spc.used_pages * 8)/1024.00 AS NUMERIC(36, 2)) AS used_mb,
	 CAST( (SUM(spc.used_pages * 8)/1024.00)/1024 as numeric(36, 2)) as used_gb,
    cast(sum(spc.total_pages * 8)/1024.00 as numeric(36, 2)) as allocated_mb,
	 cast( (SUM(spc.total_pages * 8)/1024.00) /1024.00 as numeric(36, 2)) as allocated_gb,
	 ds.name AS  filegroup_name,
	 tab.object_id as tab_object_id 
from sys.tables tab
    inner join sys.indexes ind 
        on tab.object_id = ind.object_id
    inner join sys.partitions part 
        on ind.object_id = part.object_id and ind.index_id = part.index_id
    inner join sys.allocation_units spc
        on part.partition_id = spc.container_id
INNER JOIN sys.filegroups ds ON iND.data_space_id=ds.data_space_id
	--WHERE  schema_name(tab.schema_id) = 'cdc'
--where tab.object_id = 213575799
group by schema_name(tab.schema_id) + '.' + tab.name
, ds.name 
, tab.object_id 
)
, frag as (
SELECT S.name as 'Schema',
T.name as 'Table',
I.name as 'Index',
DDIPS.avg_fragmentation_in_percent,
DDIPS.page_count,
T.object_id as Tab_object_id
FROM sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL, NULL, NULL) AS DDIPS
INNER JOIN sys.tables T on T.object_id = DDIPS.object_id
INNER JOIN sys.schemas S on T.schema_id = S.schema_id
INNER JOIN sys.indexes I ON I.object_id = DDIPS.object_id
AND DDIPS.index_id = I.index_id
WHERE DDIPS.database_id = DB_ID()
and I.name is not null
AND DDIPS.avg_fragmentation_in_percent > 0
--and  t.object_id = 213575799
)
select t.*
, f.*
from Tsize t

left join frag f on f.Tab_object_id = t.tab_object_id 
order by allocated_gb desc