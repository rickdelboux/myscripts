-- Find all data files with Percent or 1MB growth
SELECT d.name as database_name,
	mf.name as file_name,
	mf.physical_name as file_path,
	mf.type_desc as file_type,
	CONVERT(DECIMAL(20,2), (CONVERT(DECIMAL,mf.size)/128)) as filesizeMB,
	mf.growth as growth,
	'Percent' as growth_increment
	,CASE WHEN mf.max_size = -1 THEN 'Unlimited' 
	ELSE cast(mf.max_size/128 AS VARCHAR(10)) + ' mb' end AS MaxSize 
FROM sys.master_files mf 
JOIN sys.databases d  ON mf.database_id=d.database_id
WHERE is_percent_growth=1
UNION
SELECT d.name as database_name,
    mf.name as file_name,
	mf.physical_name as file_path,
    mf.type_desc as file_type,
	CONVERT(DECIMAL(20,2), (CONVERT(DECIMAL,mf.size)/128)) as filesizeMB,
	mf.growth as growth,
    --(CASE WHEN mf.growth = 128 THEN 1 END) AS growth,
	'MB' as growth_increment
	,CASE WHEN mf.max_size = -1 THEN 'Unlimited' 
	ELSE cast(mf.max_size/128 AS VARCHAR(10)) + ' mb' end AS MaxSize 
FROM sys.master_files mf 
JOIN sys.databases d ON mf.database_id=d.database_id
WHERE is_percent_growth=0
--AND mf.growth = 128
ORDER BY d.name, mf.name