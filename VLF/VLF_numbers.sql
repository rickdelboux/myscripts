USE master

 DBCC SQLPERF(logspace)


 ;WITH dbs AS (
 SELECT d.database_id
    ,ROUND(SUM(CAST(mf.size AS bigint)) * 8 / 1024, 0) Size_MBs
    ,(SUM(CAST(mf.size AS bigint)) * 8 / 1024) / 1024 AS Size_GBs
FROM sys.master_files mf
INNER JOIN sys.databases d ON d.database_id = mf.database_id
--WHERE d.database_id --> 4 -- Skip system databases
GROUP BY d.database_id
 )

 SELECT 
 d.database_id,
  [database]      = d.name, 
  [recovery]      = ls.recovery_model, 
  [vlf_count]     = ls.total_vlf_count, 
  [active_vlfs]   = ls.active_vlf_count,
  [vlf_size]      = ls.current_vlf_size_mb,
  [active_log_%]  = CONVERT(decimal(5,2), 
                    100.0*ls.active_log_size_mb/ls.total_log_size_mb)
, ls.total_log_size_mb
 ,db.Size_MBs AS DB_Size_MBs
 ,db.Size_GBs AS DB_Size_GBs
 FROM sys.databases AS d
CROSS APPLY sys.dm_db_log_stats(d.database_id) AS ls
LEFT JOIN dbs db ON db.database_id = d.database_id


SELECT * from sys.dm_db_log_info(10)
